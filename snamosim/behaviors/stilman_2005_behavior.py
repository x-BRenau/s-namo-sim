import copy
import heapq
import numpy as np
import time
from collections import OrderedDict
from shapely.geometry import Point
import random

import snamosim.behaviors.algorithms.graph_search
from .baseline_behavior import BaselineBehavior
from snamosim.behaviors.algorithms import graph_search
from snamosim.utils import utils
from snamosim.worldreps.entity_based.obstacle import Obstacle
from snamosim.worldreps.entity_based.robot import Robot
from snamosim.behaviors.algorithms.new_local_opening_check import check_new_local_opening
import snamosim.behaviors.plan.basic_actions as ba
import snamosim.behaviors.plan.action_result as ar
from snamosim.worldreps.occupation_based.binary_occupancy_grid import BinaryOccupancyGrid, BinaryInflatedOccupancyGrid
import snamosim.worldreps.occupation_based.social_topological_occupation_cost_grid as stocg
import snamosim.utils.b2_collision as b2_collision
import snamosim.utils.collision as collision
import snamosim.utils.connectivity as connectivity


class RCHConfiguration:
    def __init__(self, cell, first_obstacle_uid, first_component_uid):
        self.cell = cell
        self.first_obstacle_uid = first_obstacle_uid
        self.first_component_uid = first_component_uid

    def __eq__(self, other):
        if isinstance(other, tuple):
            return self.cell == other
        else:
            return (
                    self.cell == other.cell
                    and self.first_obstacle_uid == other.first_obstacle_uid
                    and self.first_component_uid == other.first_component_uid
            )

    def __hash__(self):
        return hash((self.cell, self.first_obstacle_uid, self.first_component_uid))


class Configuration:
    def __init__(self, floating_point_pose, polygon, cell_in_grid, fixed_precision_pose,
                 action, csv_polygon=None, bb_vertices=None):
        self.floating_point_pose = floating_point_pose
        self.polygon = polygon
        self.cell_in_grid = cell_in_grid
        self.fixed_precision_pose = fixed_precision_pose
        self.action = action
        self.csv_polygon = csv_polygon
        self.bb_vertices = bb_vertices

    def __eq__(self, other):
        if isinstance(other, graph_search.HeapNode):
            return self.fixed_precision_pose == other.element.fixed_precision_pose
        elif isinstance(other, tuple):
            return self.fixed_precision_pose == other
        else:
            return self.fixed_precision_pose == other.fixed_precision_pose

    def __hash__(self):
        return hash(self.fixed_precision_pose)


class RobotObstacleConfiguration:
    def __init__(self, robot_floating_point_pose, robot_polygon, robot_cell_in_grid, robot_fixed_precision_pose,
                 obstacle_floating_point_pose, obstacle_polygon, obstacle_cell_in_grid, obstacle_fixed_precision_pose,
                 action=None, manip_pose_id=None, robot_csv_polygon=None, robot_bb_vertices=None,
                 obstacle_csv_polygon=None, obstacle_bb_vertices=None):
        self.robot = Configuration(
            robot_floating_point_pose, robot_polygon, robot_cell_in_grid, robot_fixed_precision_pose,
            action=action, csv_polygon=robot_csv_polygon, bb_vertices=robot_bb_vertices
        )
        self.obstacle = Configuration(
            obstacle_floating_point_pose, obstacle_polygon, obstacle_cell_in_grid, obstacle_fixed_precision_pose,
            action=action, csv_polygon=obstacle_csv_polygon, bb_vertices=obstacle_bb_vertices
        )
        self.action = action
        self.manip_pose_id = manip_pose_id

    def __eq__(self, other):
        if isinstance(other, graph_search.HeapNode):
            return (
                    self.robot.fixed_precision_pose == other.element.robot.fixed_precision_pose
                    and self.obstacle.fixed_precision_pose == other.element.obstacle.fixed_precision_pose
            )
        elif isinstance(other, tuple):
            return (
                    self.robot.fixed_precision_pose == other[0]
                    and self.obstacle.fixed_precision_pose == other[1]
            )
        else:
            return (
                    self.robot.fixed_precision_pose == other.robot.fixed_precision_pose
                    and self.obstacle.fixed_precision_pose == other.obstacle.fixed_precision_pose
            )

    def __hash__(self):
        return hash((self.robot.fixed_precision_pose, self.obstacle.fixed_precision_pose))


class RobotRobotConflict:  # Systematic postpone
    def __init__(self, obstacle_uid=0):
        self.obstacle_uid = obstacle_uid

    def __str__(self):
        return "Robot-Robot conflict with obstacle uid {}.".format(self.obstacle_uid)

    def __repr__(self):
        return self.__str__()


class RobotObstacleConflict:  # Systematic immediate replan
    def __init__(self, obstacle_uid):
        self.obstacle_uid = obstacle_uid

    def __str__(self):
        return "Robot-Obstacle conflict with obstacle uid {}.".format(self.obstacle_uid)

    def __repr__(self):
        return self.__str__()


class StolenMovableConflict:  # If Movable is in grabbed state, postpone, else immediate replan
    def __init__(self, obstacle_uid):
        self.obstacle_uid = obstacle_uid

    def __str__(self):
        return "Stolen Movable conflict concerning obstacle uid {}.".format(self.obstacle_uid)

    def __repr__(self):
        return self.__str__()


class StealingMovableConflict:  # If Movable is in grabbed state, postpone, else immediate replan
    def __init__(self, obstacle_uid, thief_uid=None):
        self.obstacle_uid = obstacle_uid
        self.thief_uid = thief_uid

    def __str__(self):
        return "Stolen Movable conflict concerning obstacle uid {} by robot uid {}.".format(
            self.obstacle_uid, self.thief_uid
        )

    def __repr__(self):
        return self.__str__()


class SimultaneousSpaceAccess:  # Systematic postpone
    def __init__(self):
        pass

    def __str__(self):
        return ""

    def __repr__(self):
        return self.__str__()

class ConcurrentGrabConflict:  # Systematic postpone
    def __init__(self, obstacle_uid=0, concurrent_agents_uids=tuple()):
        self.obstacle_uid = obstacle_uid
        self.concurrent_agents_uids = concurrent_agents_uids

    def __str__(self):
        return "Concurrent Grab conflict concerning obstacle uid {}, with concurrent agents uids {}.".format(
            self.obstacle_uid, self.concurrent_agents_uids
        )

    def __repr__(self):
        return self.__str__()


class Path:
    def __init__(self, poses, polygons, cells=None, csv_polygons=None, bb_vertices=None):
        self.poses = poses
        self.polygons = polygons
        self.cells = cells
        self.csv_polygons = csv_polygons
        self.bb_vertices = bb_vertices

    # TODO Have these trans and rot precision values be passed from calling functions !
    def is_start_pose(self, pose, trans_mult=100., rot_mult=1.):
        fixed_precision_pose = utils.real_pose_to_fixed_precision_pose(pose, trans_mult, rot_mult)
        fixed_precision_self_pose = utils.real_pose_to_fixed_precision_pose(self.poses[0], trans_mult, rot_mult)
        return fixed_precision_pose == fixed_precision_self_pose


class TransferPath:
    def __init__(self, robot_path, obstacle_path, actions, grab_action, release_action, obstacle_uid, manip_pose_id,
                 phys_cost=None, social_cost=0., weight=1.):
        self.robot_path = robot_path
        self.obstacle_path = obstacle_path

        self.obstacle_uid = obstacle_uid
        self.manip_pose_id = manip_pose_id

        self.phys_cost = (
            phys_cost if phys_cost is not None
            else utils.sum_of_euclidean_distances(self.robot_path.poses) * weight
        )
        self.social_cost = social_cost
        self.total_cost = self.phys_cost + self.social_cost

        # TODO Remove this attribute that is currently kept to avoid circular dependency with ros_conversion.py
        #   Simply move this class and the other ones in another module
        self.is_transfer = True

        self.grab_action = grab_action
        self.release_action = release_action
        self.actions = actions
        self.action_index = 0

    @classmethod
    def from_configurations(cls, prev_transit_end_configuration, next_transit_start_configuration,
                            transfer_configurations, obstacle_uid, phys_cost=None, social_cost=0., weight=1.):
        if not transfer_configurations:
            return None

        manip_pose_id = transfer_configurations[0].manip_pose_id

        actions = [configuration.action for configuration in transfer_configurations if configuration.action]
        grab_action = actions[0] if prev_transit_end_configuration else None
        release_action = next_transit_start_configuration.action
        actions.append(release_action)

        robot_poses = [configuration.robot.floating_point_pose for configuration in transfer_configurations]
        robot_poses.append(next_transit_start_configuration.floating_point_pose)
        robot_polygons = [configuration.robot.polygon for configuration in transfer_configurations]
        robot_polygons.append(next_transit_start_configuration.polygon)
        robot_csv_polygons = {(i + 1,): config.robot.csv_polygon for i, config in enumerate(transfer_configurations)}
        robot_csv_polygons[(len(transfer_configurations),)] = next_transit_start_configuration
        robot_bb_vertices = [config.robot.bb_vertices for config in transfer_configurations]
        robot_bb_vertices.append(next_transit_start_configuration.bb_vertices)
        if prev_transit_end_configuration:
            robot_poses.insert(0, prev_transit_end_configuration.floating_point_pose)
            robot_polygons.insert(0, prev_transit_end_configuration.polygon)
            robot_csv_polygons[(0,)] = prev_transit_end_configuration.csv_polygon
            robot_bb_vertices.insert(0, prev_transit_end_configuration.bb_vertices)

        robot_path = Path(
            poses=robot_poses, polygons=robot_polygons, csv_polygons=robot_csv_polygons, bb_vertices=robot_bb_vertices
        )

        obstacle_path = Path(
            poses=[configuration.obstacle.floating_point_pose for configuration in transfer_configurations],
            polygons=[configuration.obstacle.polygon for configuration in transfer_configurations],
            csv_polygons={(i + 1,): config.obstacle.csv_polygon for i, config in enumerate(transfer_configurations)},
            bb_vertices=[config.obstacle.bb_vertices for config in transfer_configurations]
        )
        obstacle_path.poses.append(obstacle_path.poses[-1])
        obstacle_path.polygons.append(obstacle_path.polygons[-1])
        obstacle_path.bb_vertices.append([])
        if prev_transit_end_configuration:
            obstacle_path.poses.insert(0, obstacle_path.poses[0])
            obstacle_path.polygons.insert(0, obstacle_path.polygons[0])
            obstacle_path.bb_vertices.insert(0, [])

        return cls(
            robot_path, obstacle_path, actions, grab_action, release_action,
            obstacle_uid, manip_pose_id, phys_cost, social_cost, weight
        )

    def has_infinite_cost(self):
        return True if self.total_cost == float("inf") else False

    def is_fully_executed(self):
        return self.action_index >= len(self.actions)

    def get_conflicts(self, robot_uid, b2_sim, world, previously_moved_entities_uids,
                      check_horizon=None, use_b2=False, apply_strict_horizon=False, exit_early_for_any_conflict=False,
                      exit_early_only_for_long_term_conflicts=True):
        full_horizon = len(self.actions) - self.action_index
        if check_horizon is None:
            check_horizon = full_horizon
        elif check_horizon <= 0 and apply_strict_horizon:
            return []
        else:
            check_horizon = check_horizon

        if not use_b2:
            other_entities_polygons = {uid: e.polygon for uid, e in world.entities.items() if
                                       uid != robot_uid and uid != self.obstacle_uid and e.movability != "static"}
            other_entities_aabb_tree = collision.polygons_to_aabb_tree(other_entities_polygons)

        conflicts = []

        for counter, index in enumerate(range(self.action_index, len(self.actions))):
            if apply_strict_horizon and counter > check_horizon:
                break

            action = self.actions[index]

            if action is self.grab_action:
                robot_before_grab_pose = self.robot_path.poses[0]

                # Check that obstacle is at the expected pose (except if it supposed to be moved before that)
                if self.obstacle_uid not in previously_moved_entities_uids:
                    current_obstacle_pose = world.entities[self.obstacle_uid].pose
                    obstacle_at_start_pose = self.obstacle_path.is_start_pose(current_obstacle_pose)
                    if not obstacle_at_start_pose or self.obstacle_uid in world.entity_to_agent:
                        if self.obstacle_uid in world.entity_to_agent:
                            conflicts.append(
                                StealingMovableConflict(self.obstacle_uid, world.entity_to_agent[self.obstacle_uid])
                            )
                        else:
                            conflicts.append(StolenMovableConflict(self.obstacle_uid))
                            if exit_early_only_for_long_term_conflicts:
                                return conflicts
                        if exit_early_for_any_conflict:
                            return conflicts

                if use_b2:
                    collides_with = b2_sim.simulate_simple_kinematics(
                        [{robot_uid: self.grab_action}], init_poses={robot_uid: robot_before_grab_pose}, apply=False
                    )
                else:
                    _, collides_with, _, csv_polygons, intersections, bb_vertices = collision.csv_check_collisions(
                        main_uid=robot_uid, other_polygons=other_entities_polygons,
                        polygon_sequence=[self.robot_path.polygons[0], self.robot_path.polygons[1]],
                        action_sequence=[collision.convert_action(self.grab_action, self.robot_path.poses[0])],
                        bb_type='minimum_rotated_rectangle', aabb_tree=other_entities_aabb_tree,
                        ignored_entities=previously_moved_entities_uids
                    )
                if robot_uid in collides_with:
                    for uid in collides_with[robot_uid]:
                        if isinstance(world.entities[uid], Robot) or uid in world.entity_to_agent:
                            if counter <= check_horizon:
                                conflicts.append(RobotRobotConflict(uid))
                                if exit_early_for_any_conflict:
                                    return conflicts
                        else:
                            conflicts.append(RobotObstacleConflict(uid))
                            if exit_early_for_any_conflict or exit_early_only_for_long_term_conflicts:
                                return conflicts
            elif action is self.release_action:
                robot_before_release_pose = self.robot_path.poses[-2]
                obstacle_before_release_pose = self.obstacle_path.poses[-2]
                if use_b2:
                    collides_with = b2_sim.simulate_simple_kinematics(
                        [{robot_uid: self.release_action}],
                        init_poses={robot_uid: robot_before_release_pose, self.obstacle_uid: obstacle_before_release_pose},
                        init_welded_pairs={(robot_uid, self.obstacle_uid)},
                        apply=False
                    )
                else:
                    _, collides_with, _, csv_polygons, intersections, bb_vertices = collision.csv_check_collisions(
                        main_uid=robot_uid, other_polygons=other_entities_polygons,
                        polygon_sequence=[self.robot_path.polygons[-2], self.robot_path.polygons[-1]],
                        action_sequence=[collision.convert_action(self.release_action, self.robot_path.poses[-2])],
                        bb_type='minimum_rotated_rectangle', aabb_tree=other_entities_aabb_tree,
                        ignored_entities=previously_moved_entities_uids
                    )
                if robot_uid in collides_with:
                    for uid in collides_with[robot_uid]:
                        if isinstance(world.entities[uid], Robot) or uid in world.entity_to_agent:
                            if counter <= check_horizon:
                                conflicts.append(RobotRobotConflict(uid))
                                if exit_early_for_any_conflict:
                                    return conflicts
                        else:
                            conflicts.append(RobotObstacleConflict(uid))
                            if exit_early_for_any_conflict or exit_early_only_for_long_term_conflicts:
                                return conflicts
            else:
                if use_b2:
                    robot_pose = self.robot_path.poses[index]
                    obstacle_pose = self.obstacle_path.poses[index]
                    collides_with = b2_sim.simulate_simple_kinematics(
                        [{robot_uid: action}],
                        init_welded_pairs={(robot_uid, self.obstacle_uid)},
                        init_poses={robot_uid: robot_pose, self.obstacle_uid: obstacle_pose}, apply=False
                    )
                    if robot_uid in collides_with or self.obstacle_uid in collides_with:
                        colliding_uids = set()
                        if robot_uid in collides_with:
                            colliding_uids.update(collides_with[robot_uid])
                        if self.obstacle_uid:
                            colliding_uids.update(colliding_with[self.obstacle_uid])
                        for uid in colliding_uids:
                            if isinstance(world.entities[uid], Robot) or uid in world.entity_to_agent:
                                if counter <= check_horizon:
                                    conflicts.append(RobotRobotConflict(uid))
                                    if exit_early_for_any_conflict:
                                        return conflicts
                            else:
                                conflicts.append(RobotObstacleConflict(uid))
                                if exit_early_for_any_conflict or exit_early_only_for_long_term_conflicts:
                                    return conflicts
                else:
                    _, collides_with, _, csv_polygons, intersections, bb_vertices = collision.csv_check_collisions(
                        main_uid=robot_uid, other_polygons=other_entities_polygons,
                        polygon_sequence=self.robot_path.polygons[index:index + 2],
                        action_sequence=[collision.convert_action(self.actions[index], self.robot_path.poses[index])],
                        bb_type='minimum_rotated_rectangle', aabb_tree=other_entities_aabb_tree,
                        ignored_entities=previously_moved_entities_uids
                    )
                    if robot_uid in collides_with:
                        for uid in collides_with[robot_uid]:
                            if isinstance(world.entities[uid], Robot) or uid in world.entity_to_agent:
                                if counter <= check_horizon:
                                    conflicts.append(RobotRobotConflict(uid))
                                    if exit_early_for_any_conflict:
                                        return conflicts
                            else:
                                conflicts.append(RobotObstacleConflict(uid))
                                if exit_early_for_any_conflict or exit_early_only_for_long_term_conflicts:
                                    return conflicts
                    _, collides_with, _, csv_polygons, intersections, bb_vertices = collision.csv_check_collisions(
                        main_uid=self.obstacle_uid, other_polygons=other_entities_polygons,
                        polygon_sequence=self.obstacle_path.polygons[index:index + 2],
                        action_sequence=[collision.convert_action(self.actions[index], self.obstacle_path.poses[index])],
                        bb_type='minimum_rotated_rectangle', aabb_tree=other_entities_aabb_tree,
                        ignored_entities=previously_moved_entities_uids
                    )
                    if self.obstacle_uid in collides_with:
                        for uid in collides_with[self.obstacle_uid]:
                            if isinstance(world.entities[uid], Robot) or uid in world.entity_to_agent:
                                if counter <= check_horizon:
                                    conflicts.append(RobotRobotConflict(uid))
                                    if exit_early_for_any_conflict:
                                        return conflicts
                            else:
                                conflicts.append(RobotObstacleConflict(uid))
                                if exit_early_for_any_conflict or exit_early_only_for_long_term_conflicts:
                                    return conflicts

        return conflicts

    def pop_next_step(self):
        action = self.actions[self.action_index]
        self.action_index += 1
        return action

    def get_length(self):
        return len(self.actions)


class TransitPath:
    def __init__(self, robot_path, actions, phys_cost=None, social_cost=0., weight=1.):
        if len(robot_path.polygons) != len(robot_path.poses) != len(actions) + 1:
            raise ValueError(
                "A TransitPath requires that its polygon and pose arrays are the same size. "
                "The action array must be of this same size -1."
                "Current sizes are: polygon({}), pose({}), action({})".format(
                    len(robot_path.polygons), len(robot_path.poses), len(actions)
                )
            )

        self.robot_path = robot_path

        self.phys_cost = (
            phys_cost if phys_cost is not None
            else utils.sum_of_euclidean_distances(self.robot_path.poses) * weight
        )
        self.social_cost = social_cost
        self.total_cost = self.phys_cost + self.social_cost

        # TODO Remove this attribute that is currently kept to avoid circular dependency with ros_conversion.py
        #   Simply move this class and the other ones in another module
        self.is_transfer = False

        self.actions = actions
        self.action_index = 0

    @classmethod
    def from_poses(cls, poses, robot_polygon, robot_pose, phys_cost=None, social_cost=0., weight=1.):
        # Separate translation from rotation actions
        previous_pose, separated_poses, actions = poses[0], [poses[0]], []
        for pose in poses[1:]:
            has_rotation = not utils.angle_is_close(pose[2], previous_pose[2], rel_tol=1e-6)
            has_translation = (
                not utils.is_close(pose[0], previous_pose[0], rel_tol=1e-6)
                or not utils.is_close(pose[1], previous_pose[1], rel_tol=1e-6)
            )

            if has_rotation or has_translation:
                if has_rotation and has_translation:
                    separated_poses.append((previous_pose[0], previous_pose[1], pose[2]))
                    separated_poses.append(pose)
                else:
                    separated_poses.append(pose)
                if has_rotation:
                    actions.append(ba.Rotation(utils.get_rotation(previous_pose, pose)))
                if has_translation:
                    actions.append(
                        ba.Translation.from_absolute_translation_vector(utils.get_translation(previous_pose, pose))
                    )
            previous_pose = pose

        polygons = [utils.set_polygon_pose(robot_polygon, robot_pose, pose) for pose in separated_poses]
        robot_path = Path(separated_poses, polygons)
        return cls(robot_path, actions, phys_cost=phys_cost, social_cost=social_cost, weight=weight)

    def has_infinite_cost(self):
        return True if self.total_cost == float("inf") else False

    def is_fully_executed(self):
        return self.action_index >= len(self.actions)

    def get_conflicts(self, robot_uid, b2_sim, world, inflated_grid_by_robot,
                      check_horizon=None, use_b2=False, apply_strict_horizon=False, exit_early_for_any_conflict=False,
                      exit_early_only_for_long_term_conflicts=True):
        if not self.actions:
            return []

        full_horizon = len(self.actions) + 1 - self.action_index
        if check_horizon is None:
            check_horizon = full_horizon
        elif check_horizon <= 0 and apply_strict_horizon:
            return []
        else:
            check_horizon = check_horizon + 1

        conflicts = []

        for counter, index in enumerate(range(self.action_index, len(self.actions) + 1)):
            if apply_strict_horizon and counter > check_horizon:
                break

            pose = self.robot_path.poses[index]
            cell = utils.real_to_grid(pose[0], pose[1], inflated_grid_by_robot.res, inflated_grid_by_robot.grid_pose)
            if inflated_grid_by_robot.grid[cell[0]][cell[1]] != 0:
                colliding_obstacles = inflated_grid_by_robot.obstacles_uids_in_cell(cell)
                for uid in colliding_obstacles:
                    if isinstance(world.entities[uid], Robot) or uid in world.entity_to_agent:
                        if counter <= check_horizon:
                            conflicts.append(RobotRobotConflict(uid))
                            if exit_early_for_any_conflict:
                                return conflicts
                    else:
                        conflicts.append(RobotObstacleConflict(uid))
                        if exit_early_for_any_conflict or exit_early_only_for_long_term_conflicts:
                            return conflicts

        return conflicts

    def pop_next_step(self):
        action = self.actions[self.action_index]
        self.action_index += 1
        return action

    def get_length(self):
        return len(self.actions)


class RecoveryPath:
    def __init__(self, robot_path, actions, phys_cost=None, social_cost=0., weight=1.):
        if len(robot_path.polygons) != len(robot_path.poses) != len(actions) + 1:
            raise ValueError(
                "A TransitPath requires that its polygon and pose arrays are the same size. "
                "The action array must be of this same size -1."
                "Current sizes are: polygon({}), pose({}), action({})".format(
                    len(robot_path.polygons), len(robot_path.poses), len(actions)
                )
            )

        self.robot_path = robot_path

        self.phys_cost = (
            phys_cost if phys_cost is not None
            else utils.sum_of_euclidean_distances(self.robot_path.poses) * weight
        )
        self.social_cost = social_cost
        self.total_cost = self.phys_cost + self.social_cost

        # TODO Remove this attribute that is currently kept to avoid circular dependency with ros_conversion.py
        #   Simply move this class and the other ones in another module
        self.is_transfer = False

        self.actions = actions
        self.action_index = 0

    @classmethod
    def from_actions(cls, actions, robot, phys_cost=None, social_cost=0., weight=1.):
        current_robot_pose, current_robot_polygon = robot.pose, robot.polygon
        poses, polygons = [current_robot_pose], [current_robot_polygon]
        for action in actions:
            new_robot_pose = action.predict_pose(current_robot_pose, current_robot_pose[2])
            new_robot_polygon = action.apply(current_robot_polygon, current_robot_pose)
            poses.append(new_robot_pose)
            polygons.append(new_robot_polygon)
        robot_path = Path(poses, polygons)
        return cls(robot_path, actions, phys_cost=phys_cost, social_cost=social_cost, weight=weight)

    def has_infinite_cost(self):
        return True if self.total_cost == float("inf") else False

    def is_fully_executed(self):
        return self.action_index >= len(self.actions)

    def get_conflicts(self, robot_uid, b2_sim, world, previously_moved_entities_uids,
                      check_horizon=None, use_b2=False, apply_strict_horizon=False, exit_early_for_any_conflict=False,
                      exit_early_only_for_long_term_conflicts=True):
        full_horizon = len(self.actions) - self.action_index
        if check_horizon is None:
            check_horizon = full_horizon
        elif check_horizon <= 0 and apply_strict_horizon:
            return []
        else:
            check_horizon = check_horizon

        if not use_b2:
            other_entities_polygons = {uid: e.polygon for uid, e in world.entities.items() if
                                       uid != robot_uid and e.movability != "static"}
            other_entities_aabb_tree = collision.polygons_to_aabb_tree(other_entities_polygons)

        conflicts = []

        for counter, index in enumerate(range(self.action_index, len(self.actions))):
            if apply_strict_horizon and counter > check_horizon:
                break

            action = self.actions[index]

            if use_b2:
                robot_pose = self.robot_path.poses[index]
                collides_with = b2_sim.simulate_simple_kinematics(
                    [{robot_uid: action}], init_poses={robot_uid: robot_pose}, apply=False
                )
                if robot_uid in collides_with:
                    colliding_uids = set()
                    if robot_uid in collides_with:
                        colliding_uids.update(collides_with[robot_uid])
                    for uid in colliding_uids:
                        if isinstance(world.entities[uid], Robot) or uid in world.entity_to_agent:
                            if counter <= check_horizon:
                                conflicts.append(RobotRobotConflict(uid))
                                if exit_early_for_any_conflict:
                                    return conflicts
                        else:
                            conflicts.append(RobotObstacleConflict(uid))
                            if exit_early_for_any_conflict or exit_early_only_for_long_term_conflicts:
                                return conflicts
            else:
                _, collides_with, _, csv_polygons, intersections, bb_vertices = collision.csv_check_collisions(
                    main_uid=robot_uid, other_polygons=other_entities_polygons,
                    polygon_sequence=self.robot_path.polygons[index:index + 2],
                    action_sequence=[collision.convert_action(self.actions[index], self.robot_path.poses[index])],
                    bb_type='minimum_rotated_rectangle', aabb_tree=other_entities_aabb_tree,
                    ignored_entities=previously_moved_entities_uids
                )
                if robot_uid in collides_with:
                    for uid in collides_with[robot_uid]:
                        if isinstance(world.entities[uid], Robot) or uid in world.entity_to_agent:
                            if counter <= check_horizon:
                                conflicts.append(RobotRobotConflict(uid))
                                if exit_early_for_any_conflict:
                                    return conflicts
                        else:
                            conflicts.append(RobotObstacleConflict(uid))
                            if exit_early_for_any_conflict or exit_early_only_for_long_term_conflicts:
                                return conflicts

        return conflicts

    def pop_next_step(self):
        action = self.actions[self.action_index]
        self.action_index += 1
        return action

    def get_length(self):
        return len(self.actions)


class Plan:
    def __init__(self, path_components=[], goal=None, robot_uid=None, plan_error=None):
        self.path_components = path_components
        self.goal = goal
        self.robot_uid = robot_uid
        self.phys_cost = 0.0
        self.social_cost = 0.0
        self.total_cost = 0.0
        self.plan_error = plan_error

        self.component_index = 0

        if path_components:
            for path in path_components:
                self.phys_cost += path.phys_cost
                self.social_cost += path.social_cost
                self.total_cost += path.total_cost
        else:
            self.phys_cost = float("inf")
            self.social_cost = float("inf")
            self.total_cost = float("inf")

    def append(self, future_plan):
        self.path_components += future_plan.path_components
        self.phys_cost += future_plan.phys_cost
        self.social_cost += future_plan.social_cost
        self.total_cost += future_plan.total_cost
        return self

    def has_infinite_cost(self):
        return True if self.total_cost == float("inf") else False

    def exists(self):
        return bool(self.path_components)

    def get_conflicts(self, b2_sim, world, inflated_grid_by_robot, step_count, check_horizon=None,
                        use_b2=False, apply_strict_horizon=False, exit_early_for_any_conflict=False,
                        exit_early_only_for_long_term_conflicts=True):
        # Check validity of each component
        shared_horizon = check_horizon
        previously_moved_entities_uids = set()
        remaining_components = self.path_components[self.component_index:]
        conflicts = []
        for counter, path in enumerate(remaining_components):
            if shared_horizon is None or not apply_strict_horizon or (apply_strict_horizon and shared_horizon > 0):
                if isinstance(path, TransitPath):
                    conflicts += path.get_conflicts(
                        self.robot_uid, b2_sim, world, inflated_grid_by_robot, shared_horizon,
                        use_b2, apply_strict_horizon, exit_early_for_any_conflict,
                        exit_early_only_for_long_term_conflicts
                    )
                elif isinstance(path, RecoveryPath):
                    conflicts += path.get_conflicts(
                        self.robot_uid, b2_sim, world, previously_moved_entities_uids, shared_horizon,
                        use_b2, apply_strict_horizon, exit_early_for_any_conflict,
                        exit_early_only_for_long_term_conflicts
                    )
                elif isinstance(path, TransferPath):
                    # If the previously checked path components are valid, we assume it leaves any manipulated
                    # obstacles in the right place so we don't check again:
                    # - We simply deactivate collisions with them from the world representation
                    # - or if another path component needs to move them (check_start_pose)
                    conflicts += path.get_conflicts(
                        self.robot_uid, b2_sim, world, previously_moved_entities_uids, shared_horizon,
                        use_b2, apply_strict_horizon, exit_early_for_any_conflict,
                        exit_early_only_for_long_term_conflicts
                    )

                    previously_moved_entities_uids.add(path.obstacle_uid)
                    # inflated_grid_by_robot.deactivate_entities([path.obstacle_uid])
                    b2_sim.deactivate_entities([path.obstacle_uid])
                    inflated_grid_by_robot.deactivate_entities([path.obstacle_uid])
                else:
                    raise TypeError('Expected TransitPath or TransferPath instance.')

                if exit_early_for_any_conflict and conflicts:
                    break
                if exit_early_only_for_long_term_conflicts and conflicts:
                    is_there_long_term_conflict = any([
                        (isinstance(conflict, RobotObstacleConflict)
                            or (isinstance(conflict, StolenMovableConflict)))
                        for conflict in conflicts
                    ])
                    if is_there_long_term_conflict:
                        break

                if shared_horizon:
                    shared_horizon -= path.get_length()
            else:
                break

        # Reactivate entities that had been deactivated during checks
        # inflated_grid_by_robot.activate_entities(previously_moved_entities_uids)
        b2_sim.activate_entities(previously_moved_entities_uids)
        inflated_grid_by_robot.activate_entities(previously_moved_entities_uids)

        return conflicts

    def pop_next_step(self):
        """
        Get the next plan step to execute
        :return: the action object to be executed if there is one, None if the plan is empty
        :rtype: action or None
        :except: if pop_next_step is called when the plan is fully executed
        :exception: IndexError
        """
        current_component = self.path_components[self.component_index]
        if current_component.is_fully_executed():
            self.component_index += 1
            current_component = self.path_components[self.component_index]
        return current_component.pop_next_step()


class DynamicPlan(Plan):
    DEBUGGING_WAIT_TIME_GENERATOR = []

    def __init__(self):
        Plan.__init__(self)
        self.wait_counter = 0
        self.plan_counter = 0

        self.new_replan_history = None
        self.new_conflicts_history = {}

        self.plan_history = {}
        self.conflicts_history = {}
        self.postponements_history = {}
        self.unpostponements_history = []

    @property
    def is_postponed(self):
        return self.wait_counter > 0

    def was_last_step_success(self, w_t, last_action_result):
        # TODO Check if robot state (position and grab) are coherent with next step's preconditions
        return isinstance(last_action_result, ar.ActionSuccess)

    def get_conflicts(self, b2_sim, world, inflated_grid_by_robot, step_count, check_horizon=None,
                      use_b2=False, apply_strict_horizon=False, exit_early_for_any_conflict=False,
                      exit_early_only_for_long_term_conflicts=True):
        conflicts = Plan.get_conflicts(
            self, b2_sim, world, inflated_grid_by_robot, step_count, check_horizon=check_horizon, use_b2=use_b2,
            apply_strict_horizon=apply_strict_horizon, exit_early_for_any_conflict=exit_early_for_any_conflict,
            exit_early_only_for_long_term_conflicts=exit_early_only_for_long_term_conflicts
        )
        if conflicts:
            if step_count in self.conflicts_history:
                self.conflicts_history[step_count] += conflicts
            else:
                self.conflicts_history[step_count] = conflicts
        return conflicts

    def has_tries_remaining(self, nb_max_tries):
        if self.new_replan_history:
            return len(self.new_replan_history) < nb_max_tries
        else:
            return True

    def should_postponement_be_over(self):
        return self.wait_counter <= 1

    def can_even_be_found(self):
        if self.plan_error and self.plan_error is "start_or_goal_cell_in_static_obstacle_error":
            return False
        return True

    # Actions
    def pop_next_step(self):
        if self.is_postponed:
            self.wait_counter -= 1
            return ba.Wait()
        try:
            return Plan.pop_next_step(self)
        except Exception as e:
            print(e)

    def postpone(self, t_min, t_max, step_count):
        if self.DEBUGGING_WAIT_TIME_GENERATOR:
            self.wait_counter = self.DEBUGGING_WAIT_TIME_GENERATOR.pop(0)
        else:
            self.wait_counter = random.randint(t_min, t_max)
        self.postponements_history[step_count] = self.wait_counter

    def unpostpone(self, step_count):
        self.wait_counter = 0
        self.unpostponements_history.append(step_count)

    def update_plan(self, plan, step_count):
        self.plan_counter += 1
        if step_count in self.plan_history:
            self.plan_history[step_count].append(plan)
        else:
            self.plan_history[step_count] = [plan]

        self.wait_counter = 0

        self.path_components = plan.path_components
        self.goal = plan.goal
        self.robot_uid = plan.robot_uid
        self.phys_cost = plan.phys_cost
        self.social_cost = plan.social_cost
        self.total_cost = plan.total_cost
        self.plan_error = plan.plan_error
        self.component_index = plan.component_index


class Stilman2005Behavior(BaselineBehavior):
    def __init__(self, initial_world, robot_uid, navigation_goals, behavior_config, abs_path_to_logs_dir):
        BaselineBehavior.__init__(
            self, initial_world, robot_uid, navigation_goals, behavior_config, abs_path_to_logs_dir)

        # Configuration parameters
        parameters = behavior_config["parameters"]

        # For each, specify collision model, action space
        # self.transit_search_config =
        # self.transfer_search_config =  # Include new opening detection parameters and social cost parameters
        # self.grab_search_config =
        # self.release_search_config =
        # self.obstacle_selection_config =
        # self.plan_execution_config =

        # - Original Stilman method configuration parameters
        self.alpha = parameters["alpha_for_obstacle_choice_heur"]
        self.neighborhood = utils.CHESSBOARD_NEIGHBORHOOD  # default if bad parameter
        # self.heur_w = parameters["heuristic_cost_for_traversing_obstacle_in_choice_heur"]
        # self.basic_trans_force = parameters["basic_translation_force"]
        # self.basic_rot_moment = parameters["basic_rotation_moment"]
        self.translation_unit_cost = 1.
        self.rotation_unit_cost = 1.
        self.transfer_coefficient = 2.  # Note: MUST ALWAYS BE > 1 !
        # - Robot action space parameters
        self.angular_res = parameters["collision_check_angular_res"]
        self.rotation_unit_angle = 60.  # parameters["robot_rotation_unit_angle"]
        self.translation_unit_length = parameters["robot_translation_unit_length"]
        self.forbid_rotations = parameters["forbid_rotations"]
        self.translation_factor = self.translation_unit_cost / self.translation_unit_length
        self.rotation_factor = self.rotation_unit_cost / self.rotation_unit_angle
        self.absolute_translations = True
        self.robot_base_drive_type = "holonomic"
        # self.robot_base_drive_type = "differential"
        self.trans_mult = 1. / self._world.dd.res * 10.
        self.rot_mult = 1.

        # - S-NAMO parameters
        self.use_social_cost = parameters["use_social_cost"]
        self.bound_percentage = parameters["solution_interval_bound_percentage"]
        if parameters["manipulation_search_procedure"] == "DFS":
            if self.use_social_cost:
                self.manip_search_procedure = self.focused_manip_search
            else:
                raise ValueError("Focused manipulation search requires the use_social_cost variable to be True !")
        elif parameters["manipulation_search_procedure"] == "BFS":
            self.manip_search_procedure = self.manip_search
        self.w_social, self.w_obs, self.w_goal = 15., 10., 2.
        self.w_sum = self.w_social + self.w_obs + self.w_goal
        self.distance_to_obs_cost_is_realistic = True

        # - Extra performance parameters
        self.check_new_local_opening_before_global = parameters["check_new_local_opening_before_global"]
        self.activate_grids_logging = False  # not parameters["deactivate_grids_logging"]

        if self.robot_base_drive_type == "differential":
            self._trans_vectors = np.array([(self.translation_unit_length, 0.), (-self.translation_unit_length, 0.)])
        elif self.robot_base_drive_type == "holonomic":
            self._trans_vectors = np.array([
                (self.translation_unit_length, 0.), (-self.translation_unit_length, 0.),
                (0., self.translation_unit_length), (0., -self.translation_unit_length)
            ])

        if self.forbid_rotations:
            self._rot_angles = np.array([])
        else:
            self._rot_angles = np.array([self.rotation_unit_angle, -self.rotation_unit_angle])
        self._all_rot_angles = self.rotation_unit_angle * np.array(range(1, 360 // int(self.rotation_unit_angle)))
        self._nb_possible_angles = len(self._all_rot_angles)

        if self.absolute_translations:
            self._new_actions = []
            for trans_vector in self._trans_vectors:
                self._new_actions.append(ba.AbsoluteTranslation(trans_vector))
            for rot_angle in self._rot_angles:
                self._new_actions.append(ba.Rotation(rot_angle))
        else:
            self._new_actions = []
            for trans_vector in self._trans_vectors:
                self._new_actions.append(ba.Translation(trans_vector))
            for rot_angle in self._rot_angles:
                self._new_actions.append(ba.Rotation(rot_angle))

        self._social_costmap = None

        self.is_first_transfer_step = False

        self.check_horizon = 10

        self.angular_tolerance = .1
        self.position_tolerance = self._world.dd.res / 2.


        self.min_nb_steps_to_wait = 5
        self.max_nb_steps_to_wait = 20
        self.wait_steps = -1

        # Initialize movability status of obstacles
        for entity in self._world.entities.values():
            if entity.movability != "static":
                entity.movability = self._robot.deduce_movability(entity.type)

        # Initialize overall Box2D world simulation
        self.b2_sim = b2_collision.B2Sim(self._world.entities)

        # Initialize static obstacles occupation grid, since it is not supposed to change
        static_obs_polygons = {
            uid: entity.polygon for uid, entity in self._world.entities.items()
            if (
                (isinstance(entity, Robot) and entity.uid != self._robot_uid)
                or (isinstance(entity, Obstacle) and entity.movability == "unmovable" or entity.movability == "static")
            )
        }
        self.robot_max_inflation_radius = utils.get_circumscribed_radius(self._robot.polygon)
        self.static_obs_inf_grid = BinaryInflatedOccupancyGrid(
            static_obs_polygons, self._world.dd.res, self.robot_max_inflation_radius, neighborhood=self.neighborhood
        )
        other_entities_polygons = {
            uid: e.polygon for uid, e in self._world.entities.items() if e.uid != self._robot.uid
        }
        self.inflated_grid_by_robot = BinaryInflatedOccupancyGrid(
            other_entities_polygons, self._world.dd.res, self.robot_max_inflation_radius,
            neighborhood=self.neighborhood,
            params=self.static_obs_inf_grid.params
        ) # TODO Make sure static and generalist grid share same width and height (occurs naturally if map borders are static, but not otherwise)

        # Initialize social occupation costmap
        if self.use_social_cost and self._social_costmap is None:
            static_obs_grid = BinaryOccupancyGrid(
                static_obs_polygons, self._world.dd.res, neighborhood=self.neighborhood,
                params=self.static_obs_inf_grid.params
            )
            self._social_costmap = stocg.compute_social_costmap(
                static_obs_grid.grid, self._world.dd.res, log_costmaps=self.activate_grids_logging,
                abs_path_to_logs_dir=self.abs_path_to_logs_dir, ns=self._robot_name)
            self._rp.publish_grid_map(self._social_costmap, self._world.dd.res, ns=self._robot_name)

        self.replan_count = 10
        self.goal_to_plans = OrderedDict()

        self.action_space_reduction= 'only_r_acc_then_c_1_x'  # ['none', 'only_r_acc', 'only_r_acc_then_c_1_x']

    def are_all_goals_finished(self):
        return not self._navigation_goals and self._q_goal is None

    def is_goal_success(self, q_r):
        return all([
            utils.is_close(q_r[0], self._q_goal[0], rel_tol=self.position_tolerance),
            utils.is_close(q_r[1], self._q_goal[1], rel_tol=self.position_tolerance),
            utils.angle_is_close(q_r[2], self._q_goal[2], rel_tol=self.angular_tolerance)
        ])

    def get_current_goal(self):
        return self._q_goal

    def sense(self, ref_world, last_action_result, step_count):
        # Update baseline world representation (polygons)
        BaselineBehavior.sense(self, ref_world, last_action_result, step_count)

        # Update Box2D-based world representation
        self.b2_sim.add_entities({uid: self._world.entities[uid] for uid in self._added_uids})
        self.b2_sim.update_entities({uid: self._world.entities[uid] for uid in self._updated_uids})
        self.b2_sim.remove_entities(self._removed_uids)

        # Update grid(s)
        self.inflated_grid_by_robot.polygon_update(
            new_or_updated_polygons={
                uid: self._world.entities[uid].polygon
                for uid in self._added_uids.union(self._updated_uids) if uid != self._robot_uid
            },
            removed_polygons=self._removed_uids
        )

    def think(self):
        if self._q_goal is None:
            if self._navigation_goals:
                self._q_goal = self._navigation_goals.pop(0)  # TODO Stop popping goals, use an index
                self._p_opt = DynamicPlan()
                self.goal_to_plans[self._q_goal] = self._p_opt
            else:
                return ba.GoalsFinished()

        next_step = self.new_local_coordination_strategy(
            self._world, self.static_obs_inf_grid, self.inflated_grid_by_robot, self.b2_sim,
            self._robot_uid, self._q_goal, self._p_opt, self.check_horizon, self.replan_count,
            self.min_nb_steps_to_wait, self.max_nb_steps_to_wait, self.position_tolerance, self.angular_tolerance,
            self.neighborhood, self._step_count, self.trans_mult, self.rot_mult, self.action_space_reduction
        )

        if isinstance(next_step, (ba.GoalSuccess, ba.GoalFailed)):
            self._q_goal = None

        return next_step


    def is_goal_reached(self, q_t, q_f, pos_tol=0.05, ang_tol=0.1):
        return all([
            utils.is_close(q_t[0], q_f[0], rel_tol=pos_tol),
            utils.is_close(q_t[1], q_f[1], rel_tol=pos_tol),
            utils.angle_is_close(q_t[2], q_f[2], rel_tol=ang_tol)
        ])

    def must_replan_now(self, conflicts):
        for conflict in conflicts:
            if isinstance(conflict, (StolenMovableConflict, RobotObstacleConflict)):
                return True
        return False

    def new_local_coordination_strategy(self, w_t, static_obs_inf_grid, inflated_grid_by_robot, b2_sim,
                                        robot_uid, goal, plan, fov, try_max, t_min, t_max, pos_tol, ang_tol,
                                        neighborhood, step_count, trans_mult, rot_mult, action_space_reduction):
        # If current robot pose is close enough to goal, return Success
        if self.is_goal_reached(w_t.entities[robot_uid].pose, goal, pos_tol, ang_tol):
            return ba.GoalSuccess(goal)

        # Try to compute and/or execute a plan to reach the goal
        try_compute_plan = False
        if plan.exists():
            if plan.was_last_step_success(w_t, self._last_action_result):
                conflicts = plan.get_conflicts(b2_sim, w_t, inflated_grid_by_robot, step_count, fov)
                if plan.is_postponed:
                    if not conflicts:
                        self.simulation_log.append(utils.BasicLog(
                            "Agent {}: Unpostponing plan.".format(self._robot_name), step_count
                        ))
                        plan.unpostpone(step_count)
                        return plan.pop_next_step()
                    else:
                        if self.must_replan_now(conflicts):
                            self.simulation_log.append(utils.BasicLog(
                                "Agent {}: Try compute plan because conflicts during postponement (with {} steps left) that require immediate replanning: {}".format(self._robot_name, plan.wait_counter, conflicts), step_count
                            ))
                            plan.new_conflicts_history[step_count] = conflicts[0]
                            try_compute_plan = True
                        elif plan.should_postponement_be_over():
                            self.simulation_log.append(utils.BasicLog(
                                "Agent {}: Try compute plan because postponement should be over and there are conflicts: {}".format(self._robot_name, conflicts), step_count
                            ))
                            try_compute_plan = True
                        else:
                            return plan.pop_next_step()
                else:
                    if not conflicts:
                        return plan.pop_next_step()
                    else:
                        if self.must_replan_now(conflicts):
                            self.simulation_log.append(utils.BasicLog(
                                "Agent {}: Try compute plan because conflicts during normal execution: {}".format(self._robot_name, conflicts), step_count
                            ))
                            plan.new_conflicts_history[step_count] = conflicts[0]
                            try_compute_plan = True
                        else:
                            plan.new_conflicts_history[step_count] = conflicts[0]
                            plan.postpone(t_min, t_max, step_count)
                            self.simulation_log.append(utils.BasicLog(
                                "Agent {}: Postpone plan for {} steps because conflicts during normal execution: {}".format(self._robot_name, plan.wait_counter, conflicts), step_count
                            ))
                            return plan.pop_next_step()
            else:
                self.simulation_log.append(utils.BasicLog(
                    "Agent {}: Try compute plan because last step execution failed.".format(self._robot_name), step_count
                ))
                if isinstance(self._last_action_result, ar.DynamicCollisionFailure):
                    plan.new_conflicts_history[step_count] = SimultaneousSpaceAccess()
                else:
                    plan.new_conflicts_history[step_count] = ConcurrentGrabConflict()
                try_compute_plan = True
        else:
            if plan.is_postponed:
                return plan.pop_next_step()
            else:
                self.simulation_log.append(utils.BasicLog(
                    "Agent {}: Try compute plan because there is no plan yet.".format(self._robot_name), step_count
                ))
                try_compute_plan = True

        if try_compute_plan:
            if plan.has_tries_remaining(try_max):
                if plan.new_replan_history is None:
                    plan.new_replan_history = set()
                else:
                    plan.new_replan_history.add(step_count)
                # TODO Move all this to Plan.compute_plan method
                dynamic_entities = {
                    uid for uid, entity in w_t.entities.items() if (
                        (isinstance(entity, Robot) and uid != robot_uid)
                        or (uid in w_t.entity_to_agent and w_t.entity_to_agent[uid] != robot_uid)
                    )
                }
                w_t_no_dyn = w_t.light_copy(ignored_entities=dynamic_entities)
                inflated_grid_by_robot.deactivate_entities(dynamic_entities)
                static_obs_inf_grid.deactivate_entities(dynamic_entities)
                p = self.select_connect(
                    w_t_no_dyn, static_obs_inf_grid, inflated_grid_by_robot, b2_sim, goal, trans_mult, rot_mult,
                    neighborhood=neighborhood, action_space_reduction=action_space_reduction
                )
                inflated_grid_by_robot.activate_entities(dynamic_entities)
                static_obs_inf_grid.activate_entities(dynamic_entities)
                plan.update_plan(p, step_count)
                self._rp.cleanup_p_opt(ns=self._robot_name)
                self._rp.publish_p_opt(self._p_opt, ns=self._robot_name)

                if plan.exists():
                    conflicts = plan.get_conflicts(b2_sim, w_t, inflated_grid_by_robot, step_count, fov)
                    if conflicts:
                        self.simulation_log.append(utils.BasicLog(
                            "Agent {}: A new plan has been computed ignoring dynamic obstacles but has conflicts with them: {}".format(self._robot_name, conflicts), step_count
                        ))
                        if plan.has_tries_remaining(try_max) and plan.can_even_be_found():
                            conflicting_uids = {conflict.obstacle_uid for conflict in conflicts}
                            new_dynamic_entities = dynamic_entities.difference(conflicting_uids)
                            new_w_t_no_dyn = w_t.light_copy(ignored_entities=new_dynamic_entities)
                            inflated_grid_by_robot.deactivate_entities(new_dynamic_entities)
                            static_obs_inf_grid.deactivate_entities(new_dynamic_entities)
                            p = self.select_connect(
                                new_w_t_no_dyn, static_obs_inf_grid, inflated_grid_by_robot, b2_sim, goal, trans_mult, rot_mult,
                                neighborhood=neighborhood, action_space_reduction=action_space_reduction
                            )
                            inflated_grid_by_robot.activate_entities(new_dynamic_entities)
                            static_obs_inf_grid.activate_entities(new_dynamic_entities)
                            plan.update_plan(p, step_count)
                            self._rp.cleanup_p_opt(ns=self._robot_name)
                            self._rp.publish_p_opt(self._p_opt, ns=self._robot_name)

                            if plan.exists():
                                conflicts = plan.get_conflicts(b2_sim, w_t, inflated_grid_by_robot, step_count, fov)
                                if conflicts:
                                    self.simulation_log.append(utils.BasicLog(
                                        "Agent {}: A new plan has been computed with conflicting obstacles but still has conflicts: {}".format(self._robot_name, conflicts), step_count
                                    ))
                                    plan.new_conflicts_history[step_count] = conflicts[0]
                                    plan.postpone(t_min, t_max, step_count)
                                    self.simulation_log.append(utils.BasicLog(
                                        "Agent {}: Postpone plan for {} steps because conflicts after computation: {}".format(
                                            self._robot_name, plan.wait_counter, conflicts), step_count
                                    ))
                                return plan.pop_next_step()
                        else:
                            ba.GoalFailed(goal)
                    else:
                        return plan.pop_next_step()

                    if plan.has_tries_remaining(try_max) and plan.can_even_be_found():
                        if robot_uid in w_t.entity_to_agent.inverse:
                            obstacle_uid = w_t.entity_to_agent.inverse[robot_uid]
                            robot, obstacle = w_t.entities[robot_uid], w_t.entities[obstacle_uid]
                            other_entities_polygons = {
                                uid: e.polygon
                                for uid, e in w_t.entities.items() if uid not in (robot_uid, obstacle_uid)
                            }
                            other_entities_aabb_tree = collision.polygons_to_aabb_tree(other_entities_polygons)
                            transit_configuration_after_release = self.get_next_transit_start_configuration(
                                inflated_grid_by_robot, robot.pose, robot.polygon, robot_uid, obstacle_uid, obstacle.pose, b2_sim,
                                other_entities_polygons, other_entities_aabb_tree, trans_mult, rot_mult, use_b2=False
                            )
                            if transit_configuration_after_release:
                                self.simulation_log.append(utils.BasicLog(
                                    "Agent {}: Release object {} because of plan change during manipulation.".format(self._robot_name, obstacle.name), step_count
                                ))
                                return transit_configuration_after_release.action
                            else:
                                self.simulation_log.append(utils.BasicLog(
                                    "Agent {}: Could not release object {} during manipulation because no valid transit pose could be found.".format(self._robot_name, obstacle.name), step_count
                                ))
                        plan.new_conflicts_history[step_count] = conflicts[0]
                        plan.postpone(t_min, t_max, step_count)
                        self.simulation_log.append(utils.BasicLog(
                            "Agent {}: Postponing for {} steps because no plan could be found yet.".format(self._robot_name, plan.wait_counter), step_count
                        ))
                        return plan.pop_next_step()
                    else:
                        self.simulation_log.append(utils.BasicLog(
                            "Agent {}: No plan could be found, no tries are remaining or no plan can ever be found.".format(self._robot_name), step_count
                        ))

        # If no success nor plan step returned, return failure by default
        return ba.GoalFailed(goal)

    def select_connect(self, w_t, static_obs_inf_grid, inflated_grid_by_robot_max, b2_sim, r_f, trans_mult, rot_mult,
                       ccs_data=None, prev_list=set(), neighborhood=utils.CHESSBOARD_NEIGHBORHOOD,
                       action_space_reduction="only_r_acc_then_c_1_x"):
        """
        High Level Planner _select_connect (SC).
        It makes use of _rch and _manip_search in a greedy heuristic search with backtracking.
        It backtracks locally when the object selected by _rch cannot be moved to merge the selected c_1 in c_free.
        It backtracks globally when all the paths identified by _rch from c_1 are unsuccessful.
        SC calls _find_path to determine a transit path from r_t to a contact point, r_t_plus_1 . The existence of the
        path is guaranteed by the choice of contacts in Manip-Search.
        # :param w_t: state of the world at time t
        # :param r_f: goal robot configuration [x, y, theta] in {m, m, degrees}
        # :return: None to backtrack, current partial plan otherwise.
        """
        robot = w_t.entities[self._robot_uid]
        r_t = robot.pose

        avoid_list = set()

        robot_cell = utils.real_to_grid(r_t[0], r_t[1], static_obs_inf_grid.res, static_obs_inf_grid.grid_pose)
        goal_cell = utils.real_to_grid(r_f[0], r_f[1], static_obs_inf_grid.res, static_obs_inf_grid.grid_pose)

        ################################################################
        start_cell_occupied = inflated_grid_by_robot_max.grid[robot_cell[0]][robot_cell[1]] > 0
        robot_in_transfer = self._robot_uid in w_t.entity_to_agent.inverse
        if start_cell_occupied and not robot_in_transfer:
            free_cell_found, free_cell, _, _, _, _ = graph_search.grid_search_closest_free_cell(
                robot_cell, inflated_grid_by_robot_max.grid, inflated_grid_by_robot_max.d_width,
                inflated_grid_by_robot_max.d_height, neighborhood
            )
            if free_cell_found:
                free_pose = utils.grid_to_real(
                    free_cell[0], free_cell[1], inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose
                )
                cur_to_free_vector = (free_pose[0] - r_t[0], free_pose[1] - r_t[1])
                translation = ba.AbsoluteTranslation(cur_to_free_vector)

                if self.robot_base_drive_type == 'holonomic':
                    valid_action_sequence = [translation]
                elif self.robot_base_drive_type == 'differential':
                    forward_final_angle = utils.yaw_from_direction(dir_cur_to_free)
                    backward_final_angle = utils.angle_to_360_interval(forward_angle + 180.)
                    action_sequences = [
                        [ba.Rotation(r_t[2] - forward_final_angle), translation],
                        [ba.Rotation(r_t[2] + forward_final_angle), translation],
                        [ba.Rotation(r_t[2] - backward_final_angle), translation],
                        [ba.Rotation(r_t[2] + backward_final_angle), translation]
                    ]
                    valid_action_sequence = []
                    for action_sequence in action_sequence:
                        action_collides = False
                        for action in action_sequence:
                            collides_with = collision.csv_simulate_simple_kinematics(w_t, {robot.uid: action})
                            if collides_with:
                                action_collides = True
                                break
                        if action_collides:
                            continue
                        else:
                            valid_action_sequence = action_sequence

                if valid_action_sequence:
                    recovery_path = RecoveryPath.from_actions(valid_action_sequence, robot)

                    w_t_plus_2 = copy.deepcopy(w_t)
                    for action in valid_action_sequence:
                        collision.csv_simulate_simple_kinematics(w_t_plus_2, {robot.uid: action}, apply=True, ignore_collisions=True)

                    self.simulation_log.append(utils.BasicLog(
                        "Agent {}: select_connect: Adding recovery path from occupied cell.".format(robot.name),
                        self._step_count)
                    )
                    future_plan = self.select_connect(
                        w_t_plus_2, static_obs_inf_grid, inflated_grid_by_robot_max, self.b2_sim, r_f, trans_mult, rot_mult,
                        ccs_data=ccs_data, prev_list=prev_list, neighborhood=neighborhood, action_space_reduction=action_space_reduction
                    )
                    if not future_plan.plan_error:
                        return Plan([recovery_path], r_f, self._robot_uid).append(future_plan)
        ################################################################

        simple_path_to_goal = self.find_path(r_t, r_f, inflated_grid_by_robot_max, robot.polygon)
        if simple_path_to_goal:
            # If the goal is in the same free space component as the robot in simulated w_t
            # Orig. condition in pseudo-code is : x^f in C^acc_R(W)
            # TODO FIX COST COMPUTATION TO FIT SAME MODEL AS MANIP SEARCH !
            return Plan([simple_path_to_goal], r_f, self._robot_uid)

        if ccs_data is None:
            ccs_data = connectivity.init_ccs_for_grid(
                inflated_grid_by_robot_max.grid, inflated_grid_by_robot_max.d_width,
                inflated_grid_by_robot_max.d_height, neighborhood
            )
        connected_components_grid = ccs_data.grid
        self._rp.publish_connected_components_grid(connected_components_grid, w_t.dd, ns=robot.name)

        c_0 = ccs_data.grid[robot_cell[0]][robot_cell[1]]
        prev_list = prev_list if c_0 == 0 else prev_list.union({c_0})
        r_acc_cells = set() if inflated_grid_by_robot_max.grid[robot_cell[0]][robot_cell[1]] > 0 else connectivity.bfs_init(
            inflated_grid_by_robot_max.grid, inflated_grid_by_robot_max.d_width,
            inflated_grid_by_robot_max.d_height, robot_cell, neighborhood
        ).visited

        if inflated_grid_by_robot_max.only_obstacle_uid_in_cell(robot_cell) == -1:
            return Plan(plan_error="start_cell_in_several_movable_obstacles_error")

        if static_obs_inf_grid.grid[robot_cell[0]][robot_cell[1]] > 0 or static_obs_inf_grid.grid[goal_cell[0]][goal_cell[1]] > 0:
            return Plan(plan_error="start_or_goal_cell_in_static_obstacle_error")

        if inflated_grid_by_robot_max.grid[goal_cell[0]][goal_cell[1]] > 1:
            return Plan(plan_error="goal_cell_in_more_than_one_movable_obstacle_error")

        forbidden_obstacles = {  # Dynamic obstacles are forbidden !
            uid for uid, entity in w_t.entities.items() if (
                (isinstance(entity, Robot) and uid != self._robot.uid)
                or (uid in w_t.entity_to_agent and w_t.entity_to_agent[uid] != self._robot.uid)
            )
        }
        o_1, c_1 = self.rch(
            robot_cell, goal_cell, static_obs_inf_grid, connected_components_grid,
            inflated_grid_by_robot_max, avoid_list, prev_list, forbidden_obstacles, neighborhood
        )
        while o_1 != 0:
            self.simulation_log.append(utils.BasicLog(
                "Agent {}: select_connect: selected entity {} for manipulation search to reach component {}.".format(
                    robot.name, w_t.entities[o_1].name, c_1
                ),
                self._step_count)
            )
            if action_space_reduction == "none":
                w_t_plus_2, tho_m = self.manip_search_procedure(
                    w_t, o_1, c_1, ccs_data, r_acc_cells, r_f, b2_sim, inflated_grid_by_robot_max, trans_mult,
                    rot_mult, obstacle_can_intrude_r_acc=True, obstacle_can_intrude_c_1_x=True
                )
            elif action_space_reduction == "only_r_acc":
                w_t_plus_2, tho_m = self.manip_search_procedure(
                    w_t, o_1, c_1, ccs_data, r_acc_cells, r_f, b2_sim, inflated_grid_by_robot_max, trans_mult,
                    rot_mult, obstacle_can_intrude_r_acc=True, obstacle_can_intrude_c_1_x=False
                )
            elif action_space_reduction == "only_r_acc_then_c_1_x":
                w_t_plus_2, tho_m = self.manip_search_procedure(
                    w_t, o_1, c_1, ccs_data, r_acc_cells, r_f, b2_sim, inflated_grid_by_robot_max, trans_mult,
                    rot_mult, obstacle_can_intrude_r_acc=True, obstacle_can_intrude_c_1_x=False
                )
                if tho_m is None:
                    w_t_plus_2, tho_m = self.manip_search_procedure(
                        w_t, o_1, c_1, ccs_data, r_acc_cells, r_f, b2_sim, inflated_grid_by_robot_max, trans_mult,
                        rot_mult, obstacle_can_intrude_r_acc=False, obstacle_can_intrude_c_1_x=True
                    )
            else:
                raise ValueError(
                    'action_space_reduction variable value is {}, but it should be one of {}'.format(
                        action_space_reduction, ['none', 'only_r_acc', 'only_r_acc_then_c_1_x']
                    )
                )

            if tho_m is not None:
                self.simulation_log.append(utils.BasicLog(
                    "Agent {}: select_connect: found partial plan manipulating entity {} to reach component {}.".format(
                        robot.name, w_t.entities[o_1].name, c_1
                    ),
                    self._step_count)
                )
                prev_cells_sets = inflated_grid_by_robot_max.polygon_update({o_1: w_t_plus_2.entities[o_1].polygon})
                future_plan = self.select_connect(
                    w_t_plus_2, static_obs_inf_grid, inflated_grid_by_robot_max, self.b2_sim, r_f, trans_mult, rot_mult,
                    ccs_data=ccs_data, prev_list=(prev_list if c_1 == 0 else prev_list.union({c_1})),
                    neighborhood=neighborhood, action_space_reduction=action_space_reduction
                )
                inflated_grid_by_robot_max.cells_sets_update(prev_cells_sets)
                if not future_plan.plan_error:
                    tho_n = self.find_path(r_t, tho_m.robot_path.poses[0], inflated_grid_by_robot_max, robot.polygon)
                    if not tho_n:
                        raise ValueError("It should not be possible not to find a transit path when the transfer path is found.")
                    plan_components = [tho_n, tho_m] if tho_n.actions else [tho_m]
                    return Plan(plan_components, r_f, self._robot_uid).append(future_plan)

            # Extra check for when the goal is in a movable obstacle that we could not find how to move
            if c_1 == 0:
                self.simulation_log.append(utils.BasicLog(
                    "Agent {}: select_connect: did not find a reachable component if manipulating {}.".format(
                        robot.name, w_t.entities[o_1].name
                    ),
                    self._step_count)
                )
                break

            avoid_list.add((o_1, c_1))

            o_1, c_1 = self.rch(
                robot_cell, goal_cell, static_obs_inf_grid, connected_components_grid,
                inflated_grid_by_robot_max, avoid_list, prev_list, forbidden_obstacles, neighborhood
            )

        return Plan(plan_error="no_plan_found_error")

    def rch_get_neighbors(self, current, gscore, close_set, open_queue, came_from,
                          static_obs_grid, connected_components_grid, inflated_robot_grid,
                          avoid_list, prev_list, g_function, traversed_obstacles_ids,
                          forbidden_obstacles, neighborhood=utils.TAXI_NEIGHBORHOOD):
        """
        Combined formulation from Stilman's thesis and his article. The prevlist parameter was not used because not
        in the article and a priori not helpful. Not only that, it is not properly defined, and actually does not
        algorithmically make sense, because the C1 component gets merged with the robot accessible space as the robot
        moves obstacles around.
        """
        neighbors, tentative_gscores = [], []
        current_gscore = gscore[current]
        path_has_traversed_first_disconnected_comp = current.first_component_uid != 0
        path_has_traversed_first_obstacle = current.first_obstacle_uid != 0

        # Filter out cells that are not in the map, and in static obstacles
        candidate_neighbor_cells = utils.get_neighbors_no_coll(
            current.cell, static_obs_grid.grid, static_obs_grid.d_width, static_obs_grid.d_height, neighborhood
        )

        for neighbor_cell in candidate_neighbor_cells:
            neighbor = None
            if path_has_traversed_first_disconnected_comp:
                # Note: This validation was added according to the description in the article about not allowing
                # transitions between two different obstacles or to a cell with several obstacles, though it was not
                # explicit in the pseudocode formulation in Stilman's thesis.
                cur_cell_obs_uid = inflated_robot_grid.only_obstacle_uid_in_cell(current.cell)
                neighbor_cell_obs_uid = inflated_robot_grid.only_obstacle_uid_in_cell(neighbor_cell)

                cur_and_neighbor_not_in_mult_obs = cur_cell_obs_uid != -1 and neighbor_cell_obs_uid != -1
                current_or_neighbor_in_free_space = cur_cell_obs_uid == 0 or neighbor_cell_obs_uid == 0
                transition_is_valid = (
                    cur_and_neighbor_not_in_mult_obs
                    and (current_or_neighbor_in_free_space or cur_cell_obs_uid == neighbor_cell_obs_uid)
                    and neighbor_cell_obs_uid != current.first_obstacle_uid
                )
                if transition_is_valid:
                    neighbor = RCHConfiguration(neighbor_cell, current.first_obstacle_uid, current.first_component_uid)
            else:
                neighbor_cell_component_uid = connected_components_grid[neighbor_cell[0]][neighbor_cell[1]]
                neighbor_cell_in_free_space = inflated_robot_grid.grid[neighbor_cell[0]][neighbor_cell[1]] == 0
                if path_has_traversed_first_obstacle:
                    if neighbor_cell_in_free_space:
                        neighbor_cell_not_in_prev_component_nor_avoid_list_nor_in_init_obstacle = (
                            neighbor_cell_component_uid not in prev_list
                            and (current.first_obstacle_uid, neighbor_cell_component_uid) not in avoid_list
                            and neighbor_cell_component_uid != 0
                        )
                        if neighbor_cell_not_in_prev_component_nor_avoid_list_nor_in_init_obstacle:
                            neighbor = RCHConfiguration(
                                neighbor_cell, current.first_obstacle_uid, neighbor_cell_component_uid
                            )
                        else:
                            # Either the neighbor tries to go back to robot acc. space, or in a (obs., comp.)
                            # combination that has already been explored and for which no manip. could be found
                            pass

                    else:
                        neighbor_cell_obs_uid = inflated_robot_grid.only_obstacle_uid_in_cell(neighbor_cell)
                        if neighbor_cell_obs_uid == current.first_obstacle_uid:
                            neighbor = RCHConfiguration(neighbor_cell, current.first_obstacle_uid, 0)
                        else:
                            # Either the neighbor is in another obstacle, or in multiple, which is forbidden
                            pass
                else:
                    if neighbor_cell_in_free_space:
                        # If no obstacle has been traversed, we are still in the robot acc. space
                        neighbor = RCHConfiguration(neighbor_cell, 0, 0)
                    else:
                        neighbor_cell_obstacle_uid = inflated_robot_grid.only_obstacle_uid_in_cell(neighbor_cell)
                        if neighbor_cell_obstacle_uid > 0:
                            neighbor = RCHConfiguration(neighbor_cell, neighbor_cell_obstacle_uid, 0)
                        else:
                            # The neighbor is in multiple obstacles, which is forbidden
                            pass
            if neighbor is not None and neighbor not in close_set and neighbor.first_obstacle_uid not in forbidden_obstacles:
                neighbors.append(neighbor)
                tentative_gscores.append(
                    current_gscore + g_function(
                        current, neighbor, is_transfer=inflated_robot_grid.grid[neighbor.cell[0]][neighbor.cell[1]] > 0
                    )
                )
                traversed_obstacles_ids.add(neighbor.first_obstacle_uid)

        self._rp.publish_rch_data(
            current, gscore, close_set, open_queue, came_from, neighbors, traversed_obstacles_ids,
            inflated_robot_grid.res, inflated_robot_grid.grid_pose, ns=self._robot_name
        )

        return neighbors, tentative_gscores

    def rch(self, start_cell, goal_cell,
            static_obs_grid, connected_components_grid, inflated_robot_grid, avoid_list, prev_list, forbidden_obstacles,
            neighborhood=utils.TAXI_NEIGHBORHOOD):

        if static_obs_grid.grid[start_cell[0]][start_cell[1]] > 0:
            obstacle_names = {self._world.entities[uid].name for uid in static_obs_grid.obstacles_uids_in_cell(start_cell)}
            self.simulation_log.append(utils.BasicLog(
                'Agent {}: rch: The robot start cell {} in a rch call must always be outside of static obstacles, here: {}.'.format(self._robot_name, start_cell, obstacle_names), self._step_count)
            )
            return 0, 0

        if static_obs_grid.grid[goal_cell[0]][goal_cell[1]] > 0:
            obstacle_names = {self._world.entities[uid].name for uid in static_obs_grid.obstacles_uids_in_cell(goal_cell)}
            self.simulation_log.append(utils.BasicLog(
                'Agent {}: rch: The robot goal cell {} in a rch call must always be outside of static obstacles, here: {}.'.format(self._robot_name, goal_cell, obstacle_names), self._step_count)
            )
            return 0, 0

        start_obstacle_uid = inflated_robot_grid.only_obstacle_uid_in_cell(start_cell)
        if start_obstacle_uid == -1 or start_obstacle_uid in forbidden_obstacles:
            obstacle_names = {self._world.entities[uid].name for uid in inflated_robot_grid.obstacles_uids_in_cell(start_cell)}
            self.simulation_log.append(utils.BasicLog(
                'Agent {}: rch: The robot start cell {} in a rch call must always be at most in one obstacle and not a forbidden one, here: {}.'.format(self._robot_name, start_cell, obstacle_names), self._step_count)
            )
            return 0, 0

        if inflated_robot_grid.grid[goal_cell[0]][goal_cell[1]] > 1:
            obstacle_names = {self._world.entities[uid].name for uid in inflated_robot_grid.obstacles_uids_in_cell(goal_cell)}
            self.simulation_log.append(utils.BasicLog(
                'Agent {}: rch: The robot goal cell {} in a rch call must be at most within one movable obstacle, here: {}.'.format(self._robot_name, goal_cell, obstacle_names), self._step_count)
            )
            return 0, 0

        # TODO Create custom exceptions for above

        sqrt_of_2_times_res = utils.SQRT_OF_2 * inflated_robot_grid.res
        goal_real = utils.grid_to_real(
            goal_cell[0], goal_cell[1], inflated_robot_grid.res, inflated_robot_grid.grid_pose
        )

        def g_function(current, neighbor, is_transfer=False):
            dist = (
                sqrt_of_2_times_res
                if neighbor.cell in [
                    (current.cell[0] + i, current.cell[1] + j) for i, j in utils.CHESSBOARD_NEIGHBORHOOD_EXTRAS
                ]
                else inflated_robot_grid.res
            )
            translation_cost = self.translation_factor * dist
            return translation_cost * (1. if not is_transfer else self.transfer_coefficient)

        def h_function(_c, _g):
            translation_cost = self.translation_factor * utils.euclidean_distance(
                utils.grid_to_real(_c.cell[0], _c.cell[1], inflated_robot_grid.res, inflated_robot_grid.grid_pose),
                goal_real
            )
            return translation_cost

        traversed_obstacles_ids = utils.OrderedSet()

        def rch_get_neighbors_instance(current, gscore, close_set, open_queue, came_from):
            return self.rch_get_neighbors(
                current, gscore, close_set, open_queue, came_from,
                static_obs_grid, connected_components_grid, inflated_robot_grid,
                avoid_list, prev_list, g_function, traversed_obstacles_ids, forbidden_obstacles, neighborhood
            )

        def exit_condition(_current, _goal):
            return _current.cell == _goal.cell

        start = RCHConfiguration(start_cell, start_obstacle_uid if start_obstacle_uid > 0 else 0, 0)
        goal = RCHConfiguration(goal_cell, 0, 0)  # Note the zeroes are never used, this line is just for coherence

        path_found, end_config, _, _, _, _ = graph_search.new_generic_a_star(
            start, goal, exit_condition, rch_get_neighbors_instance, h_function
        )
        if path_found:
            if end_config.first_obstacle_uid == 0:
                raise ValueError('Rch found a path where no obstacle needed to be traversed.')
            return end_config.first_obstacle_uid, end_config.first_component_uid
        else:
            return 0, 0

    def manip_search(self, w_t, o_1, c_1, ccs_data, r_acc_cells, r_f, b2_sim, inflated_grid_by_robot_max,
                     trans_mult, rot_mult, check_new_local_opening_before_global=True,
                     obstacle_can_intrude_r_acc=True, obstacle_can_intrude_c_1_x=True):
        # Initialize manip search simulation world and some shortcut variables
        w_t_plus_2 = copy.deepcopy(w_t)

        self._rp.publish_robot_sim_world(w_t_plus_2, self._robot_uid, ns=self._robot_name)

        c_1_cells_set = set() if c_1==0 else ccs_data.ccs[c_1].visited

        res = w_t_plus_2.dd.res

        other_entities = [entity for entity in w_t_plus_2.entities.values()
                          if entity.uid != self._robot.uid and entity.uid != o_1]
        other_entities_polygons = {entity.uid: entity.polygon for entity in other_entities}
        other_entities_aabb_tree = collision.polygons_to_aabb_tree(other_entities_polygons)

        robot = w_t_plus_2.entities[self._robot.uid]
        robot_uid, robot_pose, robot_polygon, robot_name = robot.uid, robot.pose, robot.polygon, robot.name
        robot_cell = utils.real_to_grid(robot_pose[0], robot_pose[1], inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose)
        robot_min_inflation_radius = utils.get_inscribed_radius(robot_polygon)
        robot_max_inflation_radius = utils.get_circumscribed_radius(robot_polygon)

        obstacle = w_t_plus_2.entities[o_1]
        obstacle_uid, obstacle_pose, obstacle_polygon = obstacle.uid, obstacle.pose, obstacle.polygon
        obstacle_min_inflation_radius = utils.get_inscribed_radius(obstacle_polygon)

        inf_robot, inf_obstacle = copy.deepcopy(robot), copy.deepcopy(obstacle)
        inf_robot.polygon, inf_obstacle.polygon = robot.polygon.buffer(res, join_style=2), obstacle.polygon.buffer(res, join_style=2)
        b2_sim.deactivate_entities({robot_uid, obstacle_uid})
        b2_sim.add_entities({robot_uid: inf_robot, obstacle_uid: inf_obstacle})

        goal_pose, goal_cell = r_f, utils.real_to_grid(r_f[0], r_f[1], res, inflated_grid_by_robot_max.grid_pose)

        # Get accessible sampled navigation points around obstacle
        transfer_start_configs_to_cost, transfer_start_to_prev_transit_end = self.get_transfer_start_to_transit_end_and_cost(
            robot_polygon, robot_pose, robot_uid, obstacle_uid, other_entities_polygons, other_entities_aabb_tree,
            inflated_grid_by_robot_max, ccs_data, r_acc_cells,
            obstacle_pose, obstacle_polygon, trans_mult, rot_mult
        )

        if not transfer_start_configs_to_cost:
            # If there are no attainable manipulation configurations, exit early
            self._rp.cleanup_q_manips_for_obs(ns=self._robot_name)
            return w_t_plus_2, None

        # CAREFUL : We inflate by inscribed radius MINUS sqrt(2)*res to make sure occupied cells are really where the
        # entity's center should NEVER be to avoid collisions.
        # Poses in free cells of this grid may sometimes be colliding.
        inflated_grid_by_robot_min = BinaryInflatedOccupancyGrid(
            other_entities_polygons, res, robot_min_inflation_radius - utils.SQRT_OF_2 * res,
            neighborhood=utils.CHESSBOARD_NEIGHBORHOOD
        )
        inflated_grid_by_obstacle = BinaryInflatedOccupancyGrid(
            other_entities_polygons, res, obstacle_min_inflation_radius - utils.SQRT_OF_2 * res,
            neighborhood=utils.CHESSBOARD_NEIGHBORHOOD, params=inflated_grid_by_robot_max.params
        )
        # Only deactivate obstacle cells once transit end and transfer start are computed (grab action)
        inflated_grid_by_robot_max.deactivate_entities([obstacle_uid])

        # Use Dijkstra algorithm to compute a transfer path that allows for an opening to be created
        path_found, transfer_end_configuration, came_from, close_set, gscore, _ = self.dijkstra_for_manip_search(
            transfer_start_configs_to_cost, robot_uid, robot_name, obstacle_uid, obstacle_polygon,
            other_entities_polygons, other_entities_aabb_tree,
            inflated_grid_by_robot_min, inflated_grid_by_robot_max, inflated_grid_by_obstacle,
            r_acc_cells, c_1_cells_set, ccs_data, trans_mult, rot_mult, b2_sim, check_new_local_opening_before_global,
            goal_pose, goal_cell,
            obstacle_can_intrude_r_acc=obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x=obstacle_can_intrude_c_1_x
        )
        if path_found:
            self._rp.publish_sim(
                transfer_end_configuration.robot.polygon, transfer_end_configuration.obstacle.polygon,
                "/target", ns=self._robot_name
            )
            raw_path = graph_search.reconstruct_path(came_from, transfer_end_configuration)
            prev_transit_end_configuration = transfer_start_to_prev_transit_end[raw_path[0]]
            next_transit_start_configuration = self.get_next_transit_start_configuration(
                inflated_grid_by_robot_max, raw_path[-1].robot.floating_point_pose,
                raw_path[-1].robot.polygon, robot_uid, obstacle_uid, raw_path[-1].obstacle.floating_point_pose, b2_sim,
                other_entities_polygons, other_entities_aabb_tree, trans_mult, rot_mult
            )
            tho_m_phys_cost = gscore[transfer_end_configuration] + self.g(
                transfer_end_configuration.robot.floating_point_pose,
                next_transit_start_configuration.floating_point_pose,
                is_transfer=True
            )
            tho_m = TransferPath.from_configurations(
                prev_transit_end_configuration, next_transit_start_configuration,
                raw_path, obstacle_uid, tho_m_phys_cost
            )
        else:
            # If after exhausting all possible configurations, none opens a path to the connected component,
            # return None
            tho_m = None

        # Don't forget to update w_t_plus_2 with transfer end state
        if tho_m:
            robot.pose, robot.polygon = tho_m.robot_path.poses[-1], tho_m.robot_path.polygons[-1]
            obstacle.pose, obstacle.polygon = tho_m.obstacle_path.poses[-1], tho_m.obstacle_path.polygons[-1]

        self._rp.publish_robot_sim_world(w_t_plus_2, self._robot_uid, ns=self._robot_name)
        self._rp.cleanup_robot_sim(ns=self._robot_name)
        self._rp.cleanup_q_manips_for_obs(ns=self._robot_name)

        inflated_grid_by_robot_max.activate_entities([obstacle_uid])
        b2_sim.remove_entities({robot_uid, obstacle_uid})
        b2_sim.activate_entities({robot_uid, obstacle_uid})

        return w_t_plus_2, tho_m

    def focused_manip_search(self, w_t, o_1, c_1, ccs_data, r_acc_cells, r_f, b2_sim, inflated_grid_by_robot_max,
                             trans_mult, rot_mult, check_new_local_opening_before_global=True,
                             obstacle_can_intrude_r_acc=True, obstacle_can_intrude_c_1_x=True):
        # Initialize manip search simulation world and some shortcut variables
        w_t_plus_2 = copy.deepcopy(w_t)
        self._rp.publish_robot_sim_world(w_t_plus_2, self._robot_uid, ns=self._robot_name)

        c_1_cells_set = set() if c_1 == 0 else ccs_data.ccs[c_1].visited

        res = w_t_plus_2.dd.res

        other_entities = [entity for entity in w_t_plus_2.entities.values()
                          if entity.uid != self._robot.uid and entity.uid != o_1]
        other_entities_polygons = {entity.uid: entity.polygon for entity in other_entities}
        other_entities_aabb_tree = collision.polygons_to_aabb_tree(other_entities_polygons)

        robot = w_t_plus_2.entities[self._robot.uid]
        robot_uid, robot_pose, robot_name = robot.uid, robot.pose, robot.name
        robot_cell = utils.real_to_grid(robot_pose[0], robot_pose[1], inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose)
        robot_polygon = robot.polygon
        robot_min_inflation_radius = utils.get_inscribed_radius(robot_polygon)
        robot_max_inflation_radius = utils.get_circumscribed_radius(robot_polygon)

        obstacle = w_t_plus_2.entities[o_1]
        obstacle_uid, obstacle_pose= obstacle.uid, obstacle.pose
        obstacle_polygon = obstacle.polygon
        obstacle_min_inflation_radius = utils.get_inscribed_radius(obstacle_polygon)

        goal_pose, goal_cell = r_f, utils.real_to_grid(r_f[0], r_f[1], res, inflated_grid_by_robot_max.grid_pose)

        # Get accessible sampled navigation points around obstacle
        transfer_start_configs_to_cost, transfer_start_to_prev_transit_end = self.get_transfer_start_to_transit_end_and_cost(
            robot_polygon, robot_pose, robot_uid, obstacle_uid, other_entities_polygons, other_entities_aabb_tree,
            inflated_grid_by_robot_max, ccs_data, r_acc_cells,
            obstacle_pose, obstacle_polygon, trans_mult, rot_mult
        )

        if not transfer_start_configs_to_cost:
            # If there are no attainable manipulation configurations, exit early
            self._rp.cleanup_q_manips_for_obs(ns=self._robot_name)
            return w_t_plus_2, None

        # CAREFUL : We inflate by inscribed radius MINUS sqrt(2)*res to make sure occupied cells are really where the
        # entity's center should NEVER be to avoid collisions.
        # Poses in free cells of this grid may sometimes be colliding.
        inflated_grid_by_robot_min = BinaryInflatedOccupancyGrid(
            other_entities_polygons, res, robot_min_inflation_radius - utils.SQRT_OF_2 * res,
            neighborhood=utils.CHESSBOARD_NEIGHBORHOOD
        )
        inflated_grid_by_obstacle = BinaryInflatedOccupancyGrid(
            other_entities_polygons, res, obstacle_min_inflation_radius - utils.SQRT_OF_2 * res,
            neighborhood=utils.CHESSBOARD_NEIGHBORHOOD, params=inflated_grid_by_robot_max.params
        )
        inflated_grid_by_robot_max.deactivate_entities([obstacle_uid])

        # Get potentially accessible cells for obstacle ordered by associated combined costs
        cells_sorted_by_combined_cost, sorted_cell_to_combined_cost = self.new_sorted_cells_by_combined_cost(
            inflated_grid_by_obstacle, robot_polygon, robot_pose, obstacle_pose, goal_pose
        )
        bound_quantile_index = int(round(len(cells_sorted_by_combined_cost) * (1. - self.bound_percentage))) - 1
        bound_quantile_index = 0 if bound_quantile_index < 0 else bound_quantile_index
        bound_quantile = sorted_cell_to_combined_cost[cells_sorted_by_combined_cost[bound_quantile_index]]

        # 1. Find the best obstacle transfer end configuration, that is, the one with the best compromise cost
        best_transfer_end_configuration = self.find_best_transfer_end_configuration(
            robot_pose, robot_polygon, robot_name, robot_uid,
            obstacle_uid, obstacle_pose, obstacle_polygon,
            goal_pose, goal_cell, other_entities_polygons, other_entities_aabb_tree, b2_sim,
            inflated_grid_by_robot_max, cells_sorted_by_combined_cost, r_acc_cells, c_1_cells_set, ccs_data,
            transfer_start_configs_to_cost.keys(), trans_mult, rot_mult,
            gscore=None, close_set=None, check_new_local_opening_before_global=check_new_local_opening_before_global,
            obstacle_can_intrude_r_acc=obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x=obstacle_can_intrude_c_1_x
        )
        if best_transfer_end_configuration is not None:
            self._rp.publish_sim(
                best_transfer_end_configuration.robot.polygon, best_transfer_end_configuration.obstacle.polygon,
                "/target", ns=self._robot_name
            )

            # 2. If a best obstacle transfer end configuration has been found, use A Star to find a path toward it
            path_found, transfer_end_configuration, came_from, close_set, gscore, _ = self.a_star_for_manip_search(
                transfer_start_configs_to_cost, best_transfer_end_configuration,
                robot_uid, robot_name, obstacle_uid, obstacle_polygon,
                other_entities_polygons, other_entities_aabb_tree,
                inflated_grid_by_robot_min, inflated_grid_by_robot_max, inflated_grid_by_obstacle,
                r_acc_cells, c_1_cells_set, ccs_data, trans_mult, rot_mult,
                sorted_cell_to_combined_cost, bound_quantile, b2_sim, check_new_local_opening_before_global,
                goal_pose, goal_cell,
                obstacle_can_intrude_r_acc=obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x=obstacle_can_intrude_c_1_x
            )
            if path_found:
                # 3. If a path is found, return it
                self._rp.publish_sim(
                    transfer_end_configuration.robot.polygon, transfer_end_configuration.obstacle.polygon,
                    "/target", ns=self._robot_name
                )
                raw_path = graph_search.reconstruct_path(came_from, transfer_end_configuration)
                prev_transit_end_configuration = transfer_start_to_prev_transit_end[raw_path[0]]
                next_transit_start_configuration = self.get_next_transit_start_configuration(
                    inflated_grid_by_robot_max, raw_path[-1].robot.floating_point_pose,
                    raw_path[-1].robot.polygon, robot_uid, obstacle_uid, raw_path[-1].obstacle.floating_point_pose, b2_sim,
                    other_entities_polygons, other_entities_aabb_tree, trans_mult, rot_mult
                )
                tho_m_phys_cost = gscore[transfer_end_configuration] + self.g(
                    transfer_end_configuration.robot.floating_point_pose,
                    next_transit_start_configuration.floating_point_pose,
                    is_transfer=True
                )
                tho_m = TransferPath.from_configurations(
                    prev_transit_end_configuration, next_transit_start_configuration,
                    raw_path, obstacle_uid, tho_m_phys_cost
                )
            else:
                # 4. If no path is found on the first, try finding a best configuration that has a path towards it
                #   (because we assume the A Star search to have completed, giving us the paths to ALL reachable
                #   configurations.
                best_transfer_end_configuration = self.find_best_transfer_end_configuration(
                    robot_pose, robot_polygon, robot_name, robot_uid,
                    obstacle_uid, obstacle_pose, obstacle_polygon,
                    goal_pose, goal_cell, other_entities_polygons, other_entities_aabb_tree, b2_sim,
                    inflated_grid_by_robot_max, cells_sorted_by_combined_cost, r_acc_cells, c_1_cells_set, ccs_data,
                    transfer_start_configs_to_cost.keys(), trans_mult, rot_mult,
                    gscore=gscore, close_set=close_set,
                    check_new_local_opening_before_global=check_new_local_opening_before_global,
                    obstacle_can_intrude_r_acc=obstacle_can_intrude_r_acc,
                    obstacle_can_intrude_c_1_x=obstacle_can_intrude_c_1_x
                )
                if best_transfer_end_configuration is not None:
                    self._rp.publish_sim(
                        best_transfer_end_configuration.robot.polygon, best_transfer_end_configuration.obstacle.polygon,
                        "/target", ns=self._robot_name
                    )
                    raw_path = graph_search.reconstruct_path(came_from, best_transfer_end_configuration)
                    prev_transit_end_configuration = transfer_start_to_prev_transit_end[raw_path[0]]
                    next_transit_start_configuration = self.get_next_transit_start_configuration(
                        inflated_grid_by_robot_max, raw_path[-1].robot.floating_point_pose,
                        raw_path[-1].robot.polygon, robot_uid, obstacle_uid, raw_path[-1].obstacle.floating_point_pose,
                        b2_sim, other_entities_polygons, other_entities_aabb_tree, trans_mult, rot_mult
                    )
                    tho_m_phys_cost = gscore[transfer_end_configuration] + self.g(
                        best_transfer_end_configuration.robot.floating_point_pose,
                        next_transit_start_configuration.floating_point_pose,
                        is_transfer=True
                    )
                    tho_m = TransferPath.from_configurations(
                        prev_transit_end_configuration, next_transit_start_configuration,
                        raw_path, obstacle_uid, tho_m_phys_cost
                    )
                else:
                    # If after exhausting all possible configurations, none opens a path to the connected component,
                    # return None
                    tho_m = None
        else:
            # If after exhausting all possible configurations, none opens a path to the connected component,
            # return None
            tho_m = None

        # Don't forget to update w_t_plus_2 with transfer end state
        if tho_m:
            robot.pose, robot.polygon = tho_m.robot_path.poses[-1], tho_m.robot_path.polygons[-1]
            obstacle.pose, obstacle.polygon = tho_m.obstacle_path.poses[-1], tho_m.obstacle_path.polygons[-1]

        self._rp.publish_robot_sim_world(w_t_plus_2, self._robot_uid, ns=self._robot_name)
        self._rp.cleanup_robot_sim(ns=self._robot_name)
        self._rp.cleanup_q_manips_for_obs(ns=self._robot_name)

        inflated_grid_by_robot_max.activate_entities([obstacle_uid])

        return w_t_plus_2, tho_m

    def dijkstra_for_manip_search(
            self, start, robot_uid, robot_name, obstacle_uid, obstacle_polygon,
            other_entities_polygons, other_entities_aabb_tree,
            inflated_grid_by_robot_min, inflated_grid_by_robot_max, inflated_grid_by_obstacle, r_acc_cells, c_1_cells_set, ccs_data,
            trans_mult, rot_mult, b2_sim, check_new_local_opening_before_global,
            overall_goal_pose, overall_goal_cell, obstacle_can_intrude_r_acc=True, obstacle_can_intrude_c_1_x=True):

        def get_neighbors(_current, _gscore, _close_set, _open_queue, _came_from):
            return self.get_neighbors(
                _current, _gscore, _close_set, _open_queue, _came_from,
                start, inflated_grid_by_robot_min, inflated_grid_by_robot_max, inflated_grid_by_obstacle, r_acc_cells, ccs_data, robot_uid, obstacle_uid,
                trans_mult, rot_mult, b2_sim, other_entities_polygons, other_entities_aabb_tree,
                obstacle_can_intrude_r_acc=obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x=obstacle_can_intrude_c_1_x
            )

        def exit_condition(_current):
            next_transit_start_configuration = self.get_next_transit_start_configuration(
                inflated_grid_by_robot_max, _current.robot.floating_point_pose,
                _current.robot.polygon, robot_uid, obstacle_uid, _current.obstacle.floating_point_pose, b2_sim,
                other_entities_polygons, other_entities_aabb_tree, trans_mult, rot_mult
            )
            if next_transit_start_configuration:
                #   3. ... and creates a global opening to c1
                has_new_global_opening, _, _ = self.is_there_opening_to_c_1(
                    check_new_local_opening_before_global,
                    robot_name, next_transit_start_configuration.cell_in_grid,
                    obstacle_uid, obstacle_polygon, _current.obstacle.polygon,
                    other_entities_polygons, other_entities_aabb_tree, b2_sim,
                    inflated_grid_by_robot_max, c_1_cells_set,
                    overall_goal_pose, overall_goal_cell,
                    neighborhood=utils.CHESSBOARD_NEIGHBORHOOD,
                    init_blocking_areas=None, init_entity_inflated_polygon=None
                )
                if has_new_global_opening:
                    return True
            return False

        return graph_search.new_generic_dijkstra(
            start, exit_condition=exit_condition, get_neighbors=get_neighbors
        )

    def a_star_for_manip_search(self, start, goal,
                                robot_uid, robot_name, obstacle_uid, obstacle_polygon,
                                other_entities_polygons, other_entities_aabb_tree,
                                inflated_grid_by_robot_min, inflated_grid_by_robot_max,
                                inflated_grid_by_obstacle, r_acc_cells, c_1_cells_set, ccs_data,
                                trans_mult, rot_mult,
                                sorted_cell_to_combined_cost, bound_quantile, b2_sim,
                                check_new_local_opening_before_global, overall_goal_pose, overall_goal_cell,
                                obstacle_can_intrude_r_acc=True, obstacle_can_intrude_c_1_x=True):

        def get_neighbors(_current, _gscore, _close_set, _open_queue, _came_from):
            neighbors, tentative_g_scores = self.get_neighbors(
                _current, _gscore, _close_set, _open_queue, _came_from,
                start, inflated_grid_by_robot_min, inflated_grid_by_robot_max, inflated_grid_by_obstacle,
                r_acc_cells, ccs_data, robot_uid, obstacle_uid,
                trans_mult, rot_mult, b2_sim, other_entities_polygons, other_entities_aabb_tree,
                obstacle_can_intrude_r_acc=obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x=obstacle_can_intrude_c_1_x
            )
            return neighbors, tentative_g_scores

        def heuristic(_neighbor, _goal):
            return self.h(_neighbor.robot.floating_point_pose, _goal.robot.floating_point_pose)

        def flexible_exit_condition(_current, _goal):
            if _current == _goal:
                return True

            if _current.obstacle.cell_in_grid not in sorted_cell_to_combined_cost:
                # TODO Remove this TEMPORARY condition caused by sometimes missing cell in sorted_cell_to_combined_cost
                return False

            upper_bound = (1. + self.bound_percentage) * sorted_cell_to_combined_cost[_goal.obstacle.cell_in_grid]
            current_cell_cc_within_bound = sorted_cell_to_combined_cost[_current.obstacle.cell_in_grid] < upper_bound

            if current_cell_cc_within_bound:
                next_transit_start_configuration = self.get_next_transit_start_configuration(
                    inflated_grid_by_robot_max, _current.robot.floating_point_pose,
                    _current.robot.polygon, robot_uid, obstacle_uid, _current.obstacle.floating_point_pose, b2_sim,
                    other_entities_polygons, other_entities_aabb_tree, trans_mult, rot_mult
                )
                if next_transit_start_configuration:
                    #   3. ... and creates a global opening to c1
                    has_new_global_opening, _, _ = self.is_there_opening_to_c_1(
                        check_new_local_opening_before_global,
                        robot_name, next_transit_start_configuration.cell_in_grid,
                        obstacle_uid, obstacle_polygon, _current.obstacle.polygon,
                        other_entities_polygons, other_entities_aabb_tree, b2_sim,
                        inflated_grid_by_robot_max, c_1_cells_set,
                        overall_goal_pose, overall_goal_cell,
                        neighborhood=utils.CHESSBOARD_NEIGHBORHOOD,
                        init_blocking_areas=None, init_entity_inflated_polygon=None
                    )
                    if has_new_global_opening:
                        return True
            return False

        return graph_search.new_generic_a_star(
            start, goal, exit_condition=flexible_exit_condition, get_neighbors=get_neighbors, heuristic=heuristic
        )

    def get_transit_end_and_transfer_start_poses(self, obstacle_polygon, inflated_grid_by_robot_max):
        """
        For the given obstacle polygon, computes the valid transit end poses and
        corresponding valid transfer start poses:
            - Transfer start poses are at a robot inflation radius distance from the sides, and facing their middle.
            - Transit end poses are a one and a half times the grid resolution away from the obstacle's sides, so that
                their corresponding cell is **always** outside of the inflated obstacle's cells set.
                They also have the same orientation as their corresponding transfer start pose, to make the
                initialization step of the transfer path as safe as possible (the robot only has to drive a bit forward
                to touch the obstacle's side).

        TODO Add two other sampling strategies:
            - points sampled along buffered polygon
            - points sampled along lines parallel to sides, s.t. we have at least a half robot width from endpoints
        :param obstacle_polygon:
        :type obstacle_polygon:
        :param inflated_grid_by_robot_max:
        :type inflated_grid_by_robot_max:
        :return: the lists of valid transit end poses and corresponding valid transfer start poses
        :rtype: tuple(list(tuple(float, float, float)), list(tuple(float, float, float)))
        """
        candidate_transfer_start_poses = utils.sample_poses_at_middle_of_inflated_sides(
            obstacle_polygon, inflated_grid_by_robot_max.inflation_radius
        )
        candidate_transit_end_poses = utils.sample_poses_at_middle_of_inflated_sides(
            obstacle_polygon, inflated_grid_by_robot_max.inflation_radius + 1.5 * inflated_grid_by_robot_max.res
        )

        valid_transit_end_poses, valid_transfer_start_poses = [], []
        for transit_end_pose, transfer_start_pose in zip(candidate_transit_end_poses, candidate_transfer_start_poses):
            valid_transit_end_poses.append(transit_end_pose)
            valid_transfer_start_poses.append(transfer_start_pose)

        self._rp.cleanup_q_manips_for_obs(ns=self._robot_name)
        self._rp.publish_q_manips_for_obs(valid_transit_end_poses + valid_transfer_start_poses, ns=self._robot_name)

        return valid_transit_end_poses, valid_transfer_start_poses

    def get_transfer_start_to_transit_end_and_cost(self, robot_polygon, robot_pose, robot_uid, obstacle_uid,
                                                   other_entities_polygons, other_entities_aabb_tree,
                                                   inflated_grid_by_robot_max, ccs_data, r_acc_cells,
                                                   obstacle_pose, obstacle_polygon, trans_mult, rot_mult):
        robot_cell = utils.real_to_grid(
            robot_pose[0], robot_pose[1], inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose
        )
        cell_in_manip_obs = inflated_grid_by_robot_max.only_obstacle_uid_in_cell(robot_cell) == obstacle_uid

        if cell_in_manip_obs:
            # If we are in the case where the robot starts from within the inflation of the manipulated obstacle,
            # exit early with only the start transfer configuration
            transfer_start_configuration = RobotObstacleConfiguration(
                robot_floating_point_pose=robot_pose,
                robot_polygon=robot_polygon,
                robot_fixed_precision_pose=utils.real_pose_to_fixed_precision_pose(robot_pose, trans_mult, rot_mult),
                robot_cell_in_grid=utils.real_to_grid(
                    robot_pose[0], robot_pose[1], inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose
                ),
                obstacle_floating_point_pose=obstacle_pose,
                obstacle_polygon=obstacle_polygon,
                obstacle_fixed_precision_pose=utils.real_pose_to_fixed_precision_pose(
                    obstacle_pose, trans_mult, rot_mult
                ),
                obstacle_cell_in_grid=utils.real_to_grid(
                    obstacle_pose[0], obstacle_pose[1], inflated_grid_by_robot_max.res,
                    inflated_grid_by_robot_max.grid_pose
                ),
                manip_pose_id=0
            )

            transfer_start_configs_to_cost = {transfer_start_configuration: 0.}
            transfer_start_to_prev_transit_end = {transfer_start_configuration: None}

            return transfer_start_configs_to_cost, transfer_start_to_prev_transit_end

        # General case otherwise
        transit_end_robot_poses, transfer_start_robot_poses = self.get_transit_end_and_transfer_start_poses(
            obstacle_polygon, inflated_grid_by_robot_max
        )

        transfer_start_to_transit_end_robot_pose = {
            manip_pose: nav_pose for nav_pose, manip_pose in zip(transit_end_robot_poses, transfer_start_robot_poses)
        }

        transfer_start_configs_to_cost = {}
        transfer_start_to_prev_transit_end = {}
        for manip_pose_id, (transfer_start_pose, transit_end_pose) in enumerate(transfer_start_to_transit_end_robot_pose.items()):
            transit_end_cell = utils.real_to_grid(
                transit_end_pose[0], transit_end_pose[1],
                inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose
            )

            if not transit_end_cell in r_acc_cells:
                continue

            prev_transit_end_robot_polygon = utils.set_polygon_pose(robot_polygon, robot_pose, transit_end_pose)

            grab_action = ba.Grab(
                translation_vector=(utils.euclidean_distance(transfer_start_pose, transit_end_pose), 0.),
                entity_uid=obstacle_uid
            )
            transfer_start_robot_polygon = grab_action.apply(prev_transit_end_robot_polygon, transit_end_pose)

            _, collides_with, _, csv_polygons, _, bb_vertices = collision.csv_check_collisions(
                main_uid=robot_uid, other_polygons=other_entities_polygons,
                polygon_sequence=[prev_transit_end_robot_polygon, transfer_start_robot_polygon],
                action_sequence=[collision.convert_action(grab_action, transit_end_pose)],
                bb_type='minimum_rotated_rectangle', aabb_tree=other_entities_aabb_tree
            )


            if not collides_with:
                prev_transit_end_configuration = Configuration(
                    floating_point_pose=transit_end_pose, polygon=prev_transit_end_robot_polygon,
                    cell_in_grid=utils.real_to_grid(
                        transit_end_pose[0], transit_end_pose[1], inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose
                    ),
                    fixed_precision_pose=utils.real_pose_to_fixed_precision_pose(transit_end_pose, trans_mult, rot_mult),
                    action=None, csv_polygon=prev_transit_end_robot_polygon,
                    bb_vertices=list(prev_transit_end_robot_polygon.exterior.coords)
                )
                temp_transfer_start_configuration = RobotObstacleConfiguration(
                    robot_floating_point_pose=transfer_start_pose,
                    robot_polygon=utils.set_polygon_pose(robot_polygon, robot_pose, transfer_start_pose),
                    robot_fixed_precision_pose=utils.real_pose_to_fixed_precision_pose(transfer_start_pose, trans_mult, rot_mult),
                    robot_cell_in_grid=utils.real_to_grid(
                        transfer_start_pose[0], transfer_start_pose[1], inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose
                    ),
                    obstacle_floating_point_pose=obstacle_pose,
                    obstacle_polygon=obstacle_polygon,
                    obstacle_fixed_precision_pose=utils.real_pose_to_fixed_precision_pose(
                        obstacle_pose, trans_mult, rot_mult
                    ),
                    obstacle_cell_in_grid=utils.real_to_grid(
                        obstacle_pose[0], obstacle_pose[1], inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose
                    ),
                    manip_pose_id=manip_pose_id, action=grab_action,
                    robot_csv_polygon=csv_polygons[(0,)], robot_bb_vertices=bb_vertices[0],
                    obstacle_csv_polygon=obstacle_polygon, obstacle_bb_vertices=list(obstacle_polygon.exterior.coords)
                )
                transfer_start_configs_to_cost[temp_transfer_start_configuration] = self.g(transit_end_pose, transfer_start_pose, is_transfer=True)
                transfer_start_to_prev_transit_end[temp_transfer_start_configuration] = prev_transit_end_configuration

        return transfer_start_configs_to_cost, transfer_start_to_prev_transit_end

    def find_best_transfer_end_configuration(self, robot_pose, robot_polygon, robot_name, robot_uid,
                                             obstacle_uid, obstacle_pose, obstacle_polygon,
                                             goal_pose, goal_cell,
                                             other_entities_polygons, other_entities_aabb_tree, b2_sim,
                                             inflated_grid_by_robot_max, ordered_cells_by_cost, r_acc_cells, c_1_cells_set, ccs_data,
                                             init_robot_manip_configs, trans_mult, rot_mult, gscore=None, close_set=None,
                                             check_new_local_opening_before_global=True, use_b2=False,
                                             obstacle_can_intrude_r_acc=True, obstacle_can_intrude_c_1_x=True):
        if close_set:
            # If all reachable configurations have been explored, index them by obstacle cell
            obs_cell_to_reachable_configurations = {}
            for c in close_set:
                if c.obstacle.cell_in_grid in obs_cell_to_reachable_configurations:
                    obs_cell_to_reachable_configurations[c.obstacle.cell_in_grid].append(c)
                else:
                    obs_cell_to_reachable_configurations[c.obstacle.cell_in_grid] = [c]

            # Then iterate over ordered_cells_by_cost until we find a configuration that:
            while ordered_cells_by_cost:
                current_best_cell = ordered_cells_by_cost.pop()

                intrudes = self.cell_intrudes_components(
                    current_best_cell, r_acc_cells, ccs_data, obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x
                )
                if intrudes:
                    continue

                if current_best_cell in obs_cell_to_reachable_configurations:
                    #   1. Is reachable, ...
                    possible_configurations = sorted(
                        obs_cell_to_reachable_configurations[current_best_cell], key=lambda x: gscore[x]
                    )
                    for configuration in possible_configurations:
                        if not configuration.robot.polygon.within(inflated_grid_by_robot_max.aabb_polygon):
                            continue
                        if not configuration.obstacle.polygon.within(inflated_grid_by_robot_max.aabb_polygon):
                            continue

                        #   2. ... allows sufficient space for the robot to release the object, ...
                        next_transit_start_configuration = self.get_next_transit_start_configuration(
                            inflated_grid_by_robot_max, configuration.robot.floating_point_pose,
                            configuration.robot.polygon, robot_uid, obstacle_uid,
                            configuration.obstacle.floating_point_pose, b2_sim, other_entities_polygons,
                            other_entities_aabb_tree, trans_mult, rot_mult
                        )
                        if next_transit_start_configuration:
                            #   2bis. ..., does not intrude forbidden component(s), ...
                            intrudes = self.polygon_intrudes_components(
                                configuration.obstacle.polygon, inflated_grid_by_robot_max, r_acc_cells, ccs_data,
                                obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x
                            )
                            if not intrudes:
                                #   3. ... and creates a global opening to c1
                                has_new_global_opening, _, _ = self.is_there_opening_to_c_1(
                                    check_new_local_opening_before_global,
                                    robot_name, next_transit_start_configuration.cell_in_grid,
                                    obstacle_uid, obstacle_polygon, configuration.obstacle.polygon,
                                    other_entities_polygons, other_entities_aabb_tree, b2_sim,
                                    inflated_grid_by_robot_max, c_1_cells_set,
                                    goal_pose, goal_cell, neighborhood=utils.CHESSBOARD_NEIGHBORHOOD,
                                    init_blocking_areas=None, init_entity_inflated_polygon=None
                                )
                                if has_new_global_opening:
                                    return configuration
        else:
            # If we do not already have the set of reachable configurations, close_set...
            while ordered_cells_by_cost:
                # We iterate over the cells ordered by combined cost until we find a valid transfer end configuration
                current_cell = ordered_cells_by_cost[-1]

                intrudes = self.cell_intrudes_components(
                    current_cell, r_acc_cells, ccs_data, obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x
                )
                if intrudes:
                    ordered_cells_by_cost.pop()
                    continue

                # For that, we:
                for rot in [0.] + self._all_rot_angles:
                    # Iterate over the possible obstacle rotations in this cell
                    obstacle_pose_at_transfer_end = utils.grid_pose_to_real_pose(
                        list(current_cell) + [rot], inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose
                    )

                    # If the obstacle collides at this pose, don't consider checking further
                    if use_b2:
                        collides_with = b2_sim.simulate_simple_kinematics(
                            [], init_poses={obstacle_uid: obstacle_pose_at_transfer_end}
                        )
                        obstacle_transfer_end_poly = None
                    else:
                        obstacle_transfer_end_poly = utils.set_polygon_pose(
                            obstacle_polygon, obstacle_pose, obstacle_pose_at_transfer_end
                        )
                        collides_with = collision.check_static_collision(
                            obstacle_uid, obstacle_transfer_end_poly, other_entities_polygons, other_entities_aabb_tree
                        )

                    if collides_with:
                        continue

                    for init_robot_manip_config in init_robot_manip_configs:
                        # Iterate over the possible robot poses corresponding to each obstacle pose
                        robot_pose_at_transfer_end = self.deduce_robot_goal_pose(
                            init_robot_manip_config.robot.floating_point_pose, obstacle_pose, obstacle_pose_at_transfer_end
                        )

                        # For this (robot, obstacle) configuration, check if:
                        #   1. there are no static collisions for robot too, ...
                        if use_b2:
                            collides_with = b2_sim.simulate_simple_kinematics(
                                [], init_welded_pairs={(robot_uid, obstacle_uid)}, init_poses={
                                    obstacle_uid: obstacle_pose_at_transfer_end, robot_uid: robot_pose_at_transfer_end
                                }
                            )
                            robot_transfer_end_poly = None
                        else:
                            robot_transfer_end_poly = utils.set_polygon_pose(
                                robot_polygon, robot_pose, robot_pose_at_transfer_end
                            )
                            collides_with = collision.check_static_collision(
                                robot_uid, robot_transfer_end_poly, other_entities_polygons, other_entities_aabb_tree
                            )

                        if collides_with:
                            continue

                        #   2. ... the configuration allows sufficient space for the robot to release the object, ...
                        if not robot_transfer_end_poly:
                            robot_transfer_end_poly = utils.set_polygon_pose(
                                robot_polygon, robot_pose, robot_pose_at_transfer_end
                            )

                        if not robot_transfer_end_poly.within(inflated_grid_by_robot_max.aabb_polygon):
                            continue

                        next_transit_start_configuration = self.get_next_transit_start_configuration(
                            inflated_grid_by_robot_max, robot_pose_at_transfer_end,
                            robot_transfer_end_poly, robot_uid, obstacle_uid, obstacle_pose_at_transfer_end, b2_sim,
                            other_entities_polygons, other_entities_aabb_tree, trans_mult, rot_mult
                        )
                        if next_transit_start_configuration:
                            if not obstacle_transfer_end_poly:
                                obstacle_transfer_end_poly = utils.set_polygon_pose(
                                    obstacle_polygon, obstacle_pose, obstacle_pose_at_transfer_end
                                )

                            if not obstacle_transfer_end_poly.within(inflated_grid_by_robot_max.aabb_polygon):
                                continue

                            #   2bis. ..., does not intrude forbidden component(s), ...
                            intrudes = self.polygon_intrudes_components(
                                obstacle_transfer_end_poly, inflated_grid_by_robot_max, r_acc_cells, ccs_data,
                                obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x
                            )
                            if not intrudes:
                                #   3. ... and creates a global opening to c1
                                has_new_global_opening, _, _ = self.is_there_opening_to_c_1(
                                    check_new_local_opening_before_global,
                                    robot_name, next_transit_start_configuration.cell_in_grid,
                                    obstacle_uid, obstacle_polygon, obstacle_transfer_end_poly,
                                    other_entities_polygons, other_entities_aabb_tree, b2_sim,
                                    inflated_grid_by_robot_max, c_1_cells_set,
                                    goal_pose, goal_cell, neighborhood=utils.CHESSBOARD_NEIGHBORHOOD,
                                    init_blocking_areas=None, init_entity_inflated_polygon=None
                                )
                                if has_new_global_opening:
                                    return RobotObstacleConfiguration(
                                        robot_floating_point_pose=robot_pose_at_transfer_end,
                                        robot_polygon=robot_transfer_end_poly,
                                        robot_fixed_precision_pose=utils.real_pose_to_fixed_precision_pose(
                                            robot_pose_at_transfer_end, trans_mult, rot_mult
                                        ),
                                        robot_cell_in_grid=utils.real_to_grid(
                                            robot_pose_at_transfer_end[0], robot_pose_at_transfer_end[1],
                                            inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose
                                        ),
                                        obstacle_floating_point_pose=obstacle_pose_at_transfer_end,
                                        obstacle_polygon=obstacle_transfer_end_poly,
                                        obstacle_fixed_precision_pose=utils.real_pose_to_fixed_precision_pose(
                                            obstacle_pose_at_transfer_end, trans_mult, rot_mult
                                        ),
                                        obstacle_cell_in_grid=utils.real_to_grid(
                                            obstacle_pose_at_transfer_end[0], obstacle_pose_at_transfer_end[1],
                                            inflated_grid_by_robot_max.res, inflated_grid_by_robot_max.grid_pose
                                        )
                                    )
                ordered_cells_by_cost.pop()
        return None  # If no valid configuration could be found...

    @staticmethod
    def get_next_transit_start_configuration(grid, robot_pose, robot_polygon, robot_uid, obstacle_uid,
                                             obstacle_pose, b2_sim, other_entities_polygons,
                                             other_entities_aabb_tree, trans_mult, rot_mult, use_b2=False):
        release_action = ba.Release(
            translation_vector=(-1. * (grid.inflation_radius + 1.5 * grid.res), 0.), entity_uid=obstacle_uid
        )
        new_robot_pose = release_action.predict_pose(robot_pose, robot_pose[2])
        cell = utils.real_to_grid(new_robot_pose[0], new_robot_pose[1], grid.res, grid.grid_pose)

        if utils.is_in_matrix(cell, grid.d_width, grid.d_height):
            if grid.grid[cell[0]][cell[1]] > 0:
                # If the robot cell after release is in an obstacle in the grid, return False
                return None
        else:
            # If robot cell outside of grid, return False
            return None

        new_robot_polygon = release_action.apply(robot_polygon, robot_pose)

        # Check if robot is still within map bounds
        if not new_robot_polygon.within(grid.aabb_polygon):
            return None

        # Finally, we check dynamic collisions (between init configuration and after-action configuration)
        if use_b2:
            collides_with = b2_sim.simulate_simple_kinematics(
                [{robot_uid: release_action}], init_poses={robot_uid: robot_pose, obstacle_uid: obstacle_pose},
                init_welded_pairs={(robot_uid, obstacle_uid)}, apply=False
            )
        else:
            _, collides_with, _, csv_polygons, intersections, bb_vertices = collision.csv_check_collisions(
                main_uid=robot_uid, other_polygons=other_entities_polygons,
                polygon_sequence=[robot_polygon, new_robot_polygon],
                action_sequence=[collision.convert_action(release_action, robot_pose)],
                bb_type='minimum_rotated_rectangle', aabb_tree=other_entities_aabb_tree
            )

        if not collides_with:
            new_fixed_precision_pose = utils.real_pose_to_fixed_precision_pose(new_robot_pose, trans_mult, rot_mult)
            next_transit_start_configuration = Configuration(
                new_robot_pose, new_robot_polygon, cell, new_fixed_precision_pose,
                release_action, csv_polygon=csv_polygons[(0,)], bb_vertices=bb_vertices[0]
            )
            return next_transit_start_configuration
        else:
            return None

    def is_there_opening_to_c_1(self, check_new_local_opening_before_global,
                                robot_name, robot_cell,
                                obstacle_uid, old_obstacle_polygon, new_obstacle_polygon,
                                other_entities_polygons, other_entities_aabb_tree, b2_sim,
                                inflated_grid_by_robot_max, c_1_cells_set,
                                goal_pose, goal_cell, neighborhood=utils.CHESSBOARD_NEIGHBORHOOD,
                                init_blocking_areas=None, init_entity_inflated_polygon=None):
        """
        Checks if there is a path between robot_cell and a random cell in c_1_cells_set that is not covered by an
        obstacle (especially the one considered for manipulation).
        :return: True if a path is found, False otherwise
        TODO: Add proper return of init_blocking_areas and init_entity_inflated_polygon and save them in caller methods
        """
        if check_new_local_opening_before_global:
            has_new_local_opening, init_blocking_areas, init_entity_inflated_polygon = check_new_local_opening(
                old_obstacle_polygon, new_obstacle_polygon,
                other_entities_polygons, b2_sim,
                inflated_grid_by_robot_max.inflation_radius, goal_pose,
                init_blocking_areas, init_entity_inflated_polygon, robot_name
            )
        else:
            has_new_local_opening = True

        if has_new_local_opening:
            inflated_grid_by_robot_max.polygon_update(new_or_updated_polygons={obstacle_uid: new_obstacle_polygon})

            if not c_1_cells_set or (c_1_cells_set and goal_cell in c_1_cells_set):
                cell_in_c_1 = goal_cell
            else:
                c_1_cells_set_iterator = iter(c_1_cells_set)
                cell_in_c_1 = next(c_1_cells_set_iterator)
                while inflated_grid_by_robot_max.grid[cell_in_c_1[0]][cell_in_c_1[1]] != 0:
                    # While selected cell not in free space after manipulation, try another cell
                    try:
                        cell_in_c_1 = next(c_1_cells_set_iterator)
                    except StopIteration:
                        # Note: using the the exception detection is the pythonic way it seems (no has_next)
                        # No opening because c_1_cells_set is entirely inaccessible to the robot after manipulation
                        inflated_grid_by_robot_max.cells_sets_update(removed_cells_sets={obstacle_uid})
                        has_new_global_opening, skipped_global_opening_check = False, False
                        return has_new_global_opening, has_new_local_opening, skipped_global_opening_check

            # TODO Evaluate the performance change (particularly compared to Dijkstra search) if A* star had an
            #  unadmissible heuristic to hasten path discovery (or write Best-FS based solely on heuristic)
            has_new_global_opening, _, _, _, _, _ = graph_search.grid_search_a_star(
                robot_cell, cell_in_c_1, inflated_grid_by_robot_max.grid,
                inflated_grid_by_robot_max.d_width, inflated_grid_by_robot_max.d_height,
                neighborhood, check_diag_neighbors=False
            )

            inflated_grid_by_robot_max.cells_sets_update(removed_cells_sets={obstacle_uid})

            skipped_global_opening_check = False

            return has_new_global_opening, has_new_local_opening, skipped_global_opening_check
        else:
            has_new_global_opening, skipped_global_opening_check = False, True
            return has_new_global_opening, has_new_local_opening, skipped_global_opening_check

    def get_neighbors(self, current_configuration, gscore, close_set, open_queue, came_from,
                      start, inflated_grid_by_robot_min, inflated_grid_by_robot_max, inflated_grid_by_obstacle,
                      r_acc_cells, ccs_data, robot_uid, obstacle_uid,
                      trans_mult, rot_mult, b2_sim, other_entities_polygons, other_entities_aabb_tree,
                      use_b2=False, obstacle_can_intrude_r_acc=True, obstacle_can_intrude_c_1_x=True):
        """
        Creates list of neighbors that are not in close set, do not collide dynamically nor statically
        """
        # TODO Add debug display option for intersections, be it on grid(s) or in between polygons
        neighbors = []
        tentative_g_scores = []

        for action in self._new_actions:
            if isinstance(action, ba.Rotation):
                neighbor_action_opposes_prev_action = (
                    isinstance(current_configuration.action, ba.Rotation)
                    and action.angle == -1. * current_configuration.action.angle
                )
                if neighbor_action_opposes_prev_action:
                    continue

                robot_center = (
                    current_configuration.robot.floating_point_pose[0],
                    current_configuration.robot.floating_point_pose[1]
                )
                new_robot_pose = action.predict_pose(current_configuration.robot.floating_point_pose, robot_center)
                new_obstacle_pose = action.predict_pose(
                    current_configuration.obstacle.floating_point_pose, robot_center)
                extra_g_cost = self.rotation_unit_cost
            elif isinstance(action, ba.Translation):
                neighbor_action_opposes_prev_action = (
                    isinstance(current_configuration.action, ba.Translation)
                    and action.translation_vector[0] == -1. * current_configuration.action.translation_vector[0]
                    and action.translation_vector[1] == -1. * current_configuration.action.translation_vector[1]
                )
                if neighbor_action_opposes_prev_action:
                    continue

                new_robot_pose = action.predict_pose(
                    current_configuration.robot.floating_point_pose,
                    current_configuration.robot.floating_point_pose[2]
                )
                new_obstacle_pose = action.predict_pose(
                    current_configuration.obstacle.floating_point_pose,
                    current_configuration.robot.floating_point_pose[2]
                )
                extra_g_cost = self.translation_unit_cost
            else:
                raise TypeError('action must either be of type NewRotation or NewTranslation')

            # First, check whether the new configuration is in close set, if it is, ignore it
            robot_fixed_precision_pose = utils.real_pose_to_fixed_precision_pose(
                new_robot_pose, trans_mult, rot_mult
            )
            obstacle_fixed_precision_pose = utils.real_pose_to_fixed_precision_pose(
                new_obstacle_pose, trans_mult, rot_mult
            )

            if (robot_fixed_precision_pose, obstacle_fixed_precision_pose) in close_set:
                continue

            # Then check for collisions, starting at a grid level
            robot_cell_in_grid = utils.real_to_grid(
                new_robot_pose[0], new_robot_pose[1],
                inflated_grid_by_robot_min.res, inflated_grid_by_robot_min.grid_pose
            )
            obstacle_cell_in_grid = utils.real_to_grid(
                new_obstacle_pose[0], new_obstacle_pose[1],
                inflated_grid_by_obstacle.res, inflated_grid_by_obstacle.grid_pose
            )

            is_no_longer_in_grid = not (
                    utils.is_in_matrix(
                        robot_cell_in_grid, inflated_grid_by_robot_min.d_width, inflated_grid_by_robot_min.d_height
                    )
                    and utils.is_in_matrix(
                        obstacle_cell_in_grid, inflated_grid_by_obstacle.d_width, inflated_grid_by_obstacle.d_height
                    )
            )
            if is_no_longer_in_grid:
                continue
            if inflated_grid_by_robot_min.grid[robot_cell_in_grid[0]][robot_cell_in_grid[1]] != 0:
                continue
            if inflated_grid_by_obstacle.grid[obstacle_cell_in_grid[0]][obstacle_cell_in_grid[1]] != 0:
                continue

            # Continue at static polygon level, check if still in map
            new_robot_polygon = action.apply(
                current_configuration.robot.polygon, current_configuration.robot.floating_point_pose)

            # Check if robot is still within map bounds
            if not new_robot_polygon.within(inflated_grid_by_robot_min.aabb_polygon):
                continue

            new_obstacle_polygon = action.apply(
                current_configuration.obstacle.polygon, current_configuration.robot.floating_point_pose)

            # Check if obstacle is still within map bounds
            if not new_obstacle_polygon.within(inflated_grid_by_obstacle.aabb_polygon):
                continue

            # Finally, we check dynamic collisions (between init configuration and after-action configuration)
            if use_b2:
                collides_with, poses = b2_sim.simulate_simple_kinematics(
                    [{robot_uid: action}], init_welded_pairs={(robot_uid, obstacle_uid)}, apply=False,
                    init_poses={
                        robot_uid: current_configuration.robot.floating_point_pose,
                        obstacle_uid: current_configuration.obstacle.floating_point_pose
                    }, return_poses_before_reset=True
                )
                if collides_with:
                    continue
                new_robot_pose, new_obstacle_pose = poses[robot_uid], poses[obstacle_uid]
            else:
                _, collides_with, _, robot_csv_polygons, _, robot_bb_vertices = collision.csv_check_collisions(
                    main_uid=robot_uid, other_polygons=other_entities_polygons,
                    polygon_sequence=[current_configuration.robot.polygon, new_robot_polygon],
                    action_sequence=[collision.convert_action(action, current_configuration.robot.floating_point_pose)],
                    bb_type='minimum_rotated_rectangle', aabb_tree=other_entities_aabb_tree
                )
                if collides_with:
                    continue
                # TODO Refactor collision.csv_check_collisions to check for any number of attached polygons or make new function
                _, collides_with, _, obstacle_csv_polygons, _, obstacle_bb_vertices = collision.csv_check_collisions(
                    main_uid=obstacle_uid, other_polygons=other_entities_polygons,
                    polygon_sequence=[current_configuration.obstacle.polygon, new_obstacle_polygon],
                    action_sequence=[collision.convert_action(action, current_configuration.obstacle.floating_point_pose)],
                    bb_type='minimum_rotated_rectangle', aabb_tree=other_entities_aabb_tree
                )
                if collides_with:
                    continue

            # If option is activated, check that obstacle intruded the appropriate component(s)
            intrudes = self.polygon_intrudes_components(
                new_obstacle_polygon, inflated_grid_by_robot_max, r_acc_cells, ccs_data,
                obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x
            )
            if intrudes:
                continue

            # If we are here, then this newly computed neighbor configuration is valid and we must save it
            neighbor_configuration = RobotObstacleConfiguration(
                robot_floating_point_pose=new_robot_pose, robot_polygon=new_robot_polygon,
                robot_fixed_precision_pose=robot_fixed_precision_pose, robot_cell_in_grid=robot_cell_in_grid,
                obstacle_floating_point_pose=new_obstacle_pose, obstacle_polygon=new_obstacle_polygon,
                obstacle_fixed_precision_pose=obstacle_fixed_precision_pose,
                obstacle_cell_in_grid=obstacle_cell_in_grid, action=action,
                manip_pose_id=current_configuration.manip_pose_id,
                robot_csv_polygon=robot_csv_polygons[(0,)], robot_bb_vertices=robot_bb_vertices[0],
                obstacle_csv_polygon=obstacle_csv_polygons[(0,)], obstacle_bb_vertices=obstacle_bb_vertices[0]
            )

            neighbors.append(neighbor_configuration)
            tentative_g_scores.append(
                gscore[current_configuration] + extra_g_cost
            )

        self._rp.publish_manip_search_data(
            current_configuration, gscore, close_set, open_queue, came_from, neighbors, start,
            inflated_grid_by_robot_min.res, inflated_grid_by_robot_min.grid_pose, ns=self._robot_name
        )

        return neighbors, tentative_g_scores

    def find_path(self, robot_pose, goal_pose, inflated_grid_by_robot, robot_polygon):
        real_path = graph_search.real_to_grid_search_a_star(robot_pose, goal_pose, inflated_grid_by_robot)
        if real_path:
            phys_cost = 0.
            raw_path_iterator = iter(real_path)
            prev_step = next(raw_path_iterator)
            for cur_step in raw_path_iterator:
                phys_cost += self.g(prev_step, cur_step, is_transfer=False)
            return TransitPath.from_poses(real_path, robot_polygon, robot_pose, phys_cost)
        else:
            return None

    @staticmethod
    def polygon_intrudes_components(new_obstacle_polygon, inflated_grid_by_robot, r_acc_cells, ccs_data,
                                    obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x):
        if obstacle_can_intrude_r_acc and obstacle_can_intrude_c_1_x:
            return False
        elif obstacle_can_intrude_r_acc and not obstacle_can_intrude_c_1_x:
            new_obstacle_exterior_cells = utils.accurate_rasterize_in_grid(
                new_obstacle_polygon.buffer(inflated_grid_by_robot.inflation_radius),
                inflated_grid_by_robot.res, inflated_grid_by_robot.grid_pose,
                inflated_grid_by_robot.d_width, inflated_grid_by_robot.d_height, fill=False
            )
            for cell in new_obstacle_exterior_cells:
                if ccs_data.grid[cell[0]][cell[1]] > 0 and cell not in r_acc_cells:
                    return True
        elif not obstacle_can_intrude_r_acc and obstacle_can_intrude_c_1_x:
            new_obstacle_exterior_cells = utils.accurate_rasterize_in_grid(
                new_obstacle_polygon.buffer(inflated_grid_by_robot.inflation_radius),
                inflated_grid_by_robot.res, inflated_grid_by_robot.grid_pose,
                inflated_grid_by_robot.d_width, inflated_grid_by_robot.d_height, fill=False
            )
            for cell in new_obstacle_exterior_cells:
                if cell in r_acc_cells:
                    return True
        elif not obstacle_can_intrude_r_acc and not obstacle_can_intrude_c_1_x:
            return True

        return False

    @staticmethod
    def cell_intrudes_components(cell, r_acc_cells, ccs_data, obstacle_can_intrude_r_acc, obstacle_can_intrude_c_1_x):
        if obstacle_can_intrude_r_acc and obstacle_can_intrude_c_1_x:
            return False
        elif obstacle_can_intrude_r_acc and not obstacle_can_intrude_c_1_x:
            if ccs_data.grid[cell[0]][cell[1]] > 0 and cell not in r_acc_cells:
                return True
        elif not obstacle_can_intrude_r_acc and obstacle_can_intrude_c_1_x:
            if cell in r_acc_cells:
                return True
        elif not obstacle_can_intrude_r_acc and not obstacle_can_intrude_c_1_x:
            return True

        return False

    @staticmethod
    def deduce_robot_goal_pose(robot_manip_pose, obs_init_pose, obs_goal_pose):
        translation, rotation = utils.get_translation_and_rotation(obs_init_pose, obs_goal_pose)
        robot_goal_point = list(utils.rotate_then_translate_polygon(
                Point((robot_manip_pose[0], robot_manip_pose[1])), translation, rotation,
                (obs_init_pose[0], obs_init_pose[1])
            ).coords[0])
        orientation = (robot_manip_pose[2] + rotation) % 360.
        orientation = orientation if orientation >= 0. else orientation + 360.
        return robot_goal_point[0], robot_goal_point[1], orientation

    @staticmethod
    def dijkstra_cc_and_cost(start_cell, grid, res, neighborhood=utils.CHESSBOARD_NEIGHBORHOOD):
        straight_dist = res
        diag_dist = res * utils.SQRT_OF_2
        width, height = grid.shape

        frontier = []
        heapq.heappush(frontier, (0., start_cell))
        cost_so_far = {start_cell: 0.}

        while frontier:
            current = heapq.heappop(frontier)[1]
            for neighbor in utils.get_neighbors_no_coll(current, grid, width, height, neighborhood):
                extra_cost = straight_dist if current[0] == neighbor[0] or current[1] == neighbor[1] else diag_dist
                new_cost = cost_so_far[current] + extra_cost
                if neighbor not in cost_so_far or new_cost < cost_so_far[neighbor]:
                    cost_so_far[neighbor] = new_cost
                    heapq.heappush(frontier, (new_cost, neighbor))

        return cost_so_far

    def new_sorted_cells_by_combined_cost(self, inflated_grid_by_obstacle,
                                          robot_polygon, robot_pose,
                                          obstacle_pose, goal_pose):
        # Initialize some need variables
        obstacle_cell = utils.real_to_grid(
            obstacle_pose[0], obstacle_pose[1],
            inflated_grid_by_obstacle.res, inflated_grid_by_obstacle.grid_pose
        )

        robot_poly_at_goal = utils.set_polygon_pose(
            robot_polygon, robot_pose, goal_pose
        )

        robot_cells_at_goal = utils.accurate_rasterize_in_grid(
            robot_poly_at_goal, inflated_grid_by_obstacle.res,
            inflated_grid_by_obstacle.grid_pose,
            inflated_grid_by_obstacle.d_width, inflated_grid_by_obstacle.d_height, fill=True
        )

        # Compute set of potentially reachable cells for obstacle and a heuristic cost to join them
        cell_to_cost = self.dijkstra_cc_and_cost(
            obstacle_cell, inflated_grid_by_obstacle.grid,
            inflated_grid_by_obstacle.res, neighborhood=utils.CHESSBOARD_NEIGHBORHOOD
        )
        for cell in robot_cells_at_goal:
            if cell in cell_to_cost:
                del cell_to_cost[cell]

        # Filter cells where social == -1.
        for cell in list(cell_to_cost.keys()):
            if self._social_costmap[cell[0]][cell[1]] == -1.:
                del cell_to_cost[cell]

        acc_cells_for_obs, distance_cost = list(cell_to_cost.keys()), np.array(list(cell_to_cost.values()))

        social_cost = np.array([
            self._social_costmap[cell[0]][cell[1]]
            for cell in acc_cells_for_obs])

        if not self.distance_to_obs_cost_is_realistic:
            distance_cost = np.array([
                utils.euclidean_distance(
                    utils.grid_to_real(
                        cell[0], cell[1], inflated_grid_by_obstacle.res, inflated_grid_by_obstacle.grid_pose
                    ), obstacle_pose
                )
                for cell in acc_cells_for_obs])

        distance_to_goal = np.array([
            utils.euclidean_distance(
                utils.grid_to_real(
                    cell[0], cell[1], inflated_grid_by_obstacle.res, inflated_grid_by_obstacle.grid_pose
                ), goal_pose
            )
            for cell in acc_cells_for_obs])

        normalized_social_cost = (social_cost - np.min(social_cost)) / np.ptp(social_cost)
        normalized_distance_cost = (distance_cost - np.min(distance_cost)) / np.ptp(distance_cost)
        normalized_distance_to_goal = (distance_to_goal - np.min(distance_to_goal)) / np.ptp(distance_to_goal)

        combined_cost = (self.w_social * normalized_social_cost
                         + self.w_obs * normalized_distance_cost
                         + self.w_goal * normalized_distance_to_goal) / self.w_sum

        sorted_cell_to_combined_cost = OrderedDict(
            sorted(zip(acc_cells_for_obs, combined_cost), key=lambda t: t[1], reverse=True)
        )

        cells_sorted_by_combined_cost = list(sorted_cell_to_combined_cost.keys())

        self._rp.cleanup_multigoal_a_star_close_set(ns=self._robot_name)
        self._rp.cleanup_grid_map(ns=self._robot_name)

        # TODO Rewrite display functions to only display what's relevant
        # self._rp.publish_combined_costmap(sorted_cell_to_combined_cost, dd, ns=self._robot_name)

        if self.activate_grids_logging:
            stocg.display_or_log(
                np.invert(
                    inflated_grid_by_obstacle.grid.astype(np.bool)
                ),
                "-obs_inf_grid", time.strftime("%Y-%m-%d-%Hh%Mm%Ss"),
                debug_display=False, log_costmaps=True, abs_path_to_logs_dir=self.abs_path_to_logs_dir)

            normalized_social_cost_costmap = np.zeros(
                (inflated_grid_by_obstacle.d_width, inflated_grid_by_obstacle.d_height)
            )
            normalized_distance_from_obs_costmap = np.zeros(
                (inflated_grid_by_obstacle.d_width, inflated_grid_by_obstacle.d_height)
            )
            normalized_distance_from_goal_costmap = np.zeros(
                (inflated_grid_by_obstacle.d_width, inflated_grid_by_obstacle.d_height)
            )

            for i in range(len(acc_cells_for_obs)):
                cell = acc_cells_for_obs[i]
                normalized_social_cost_costmap[cell[0]][cell[1]] = normalized_social_cost[i]
                normalized_distance_from_obs_costmap[cell[0]][cell[1]] = normalized_distance_cost[i]
                normalized_distance_from_goal_costmap[cell[0]][cell[1]] = normalized_distance_to_goal[i]

            stocg.display_or_log(
                normalized_social_cost_costmap, "-n_social_costmap", time.strftime("%Y-%m-%d-%Hh%Mm%Ss"),
                debug_display=False, log_costmaps=True, abs_path_to_logs_dir=self.abs_path_to_logs_dir)
            stocg.display_or_log(
                normalized_distance_from_obs_costmap, "-n_d_to_obs_costmap", time.strftime("%Y-%m-%d-%Hh%Mm%Ss"),
                debug_display=False, log_costmaps=True, abs_path_to_logs_dir=self.abs_path_to_logs_dir)
            stocg.display_or_log(
                normalized_distance_from_goal_costmap, "-n_d_to_goal_costmap", time.strftime("%Y-%m-%d-%Hh%Mm%Ss"),
                debug_display=False, log_costmaps=True, abs_path_to_logs_dir=self.abs_path_to_logs_dir)

            combined_costmap = np.zeros((inflated_grid_by_obstacle.d_width, inflated_grid_by_obstacle.d_height))
            for cell, combined_cost in sorted_cell_to_combined_cost.items():
                combined_costmap[cell[0]][cell[1]] = combined_cost
            stocg.display_or_log(
                combined_costmap, "-combined_costmap", time.strftime("%Y-%m-%d-%Hh%Mm%Ss"),
                debug_display=False, log_costmaps=True, abs_path_to_logs_dir=self.abs_path_to_logs_dir)

        return cells_sorted_by_combined_cost, sorted_cell_to_combined_cost

    def h(self, r_i, r_j):
        translation_cost = self.translation_factor * utils.euclidean_distance(r_j, r_i)
        # rotation_cost = self.rotation_factor * (abs(r_j[2] - r_i[2]) % 180.)
        return translation_cost  # + rotation_cost

    def g(self, r_i, r_j, is_transfer=False):
        translation_cost = self.translation_factor * utils.euclidean_distance(r_j, r_i)
        rotation_cost = self.rotation_factor * abs(r_j[2] - r_i[2])
        return (translation_cost + rotation_cost) * (1. if not is_transfer else self.transfer_coefficient)
